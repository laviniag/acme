<?php
session_start();
require (__DIR__ . '/../vendor/autoload.php');
Dotenv::load(__DIR__ . '/../');
require (__DIR__ . '/basics/db.php');

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$router = new AltoRouter();
?>
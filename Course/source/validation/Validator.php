<?php
namespace Acme\Validation;
use Respect\Validation\Validator as Valid;

class Validator
{  
    public function isValid (array $load_var)
    {
        $errors = [];
        foreach ($load_var as $name => $data) {
        
            $verif_rule = explode("|", $data->rules);
            
            foreach ($verif_rule as $verif) {
                $terms = explode(":", $verif);
                $rule = $terms[0];
                $refer = (sizeof($terms) > 1) ? $terms[1] : '';

                switch ($rule) {
                    case 'req':
                        if ($data->value == '') {
                            $errors[] = "$data->label is a required input";
                            break 2;            // goto next input
                        }
                        break;
                        
                    case 'minlen':
                        if (Valid::length($refer)->Validate($data->value) == false) {
                            $errors[] = "$data->label must be at least $refer chars long";
                        }
                        break; 
                                               
                    case 'maxlen':
                        if (Valid::length($refer)->Validate($data->value) == true) {
                            $errors[] = "$data->label must be less than $refer chars long";
                        }
                        break;
                        
                    case 'minval':
                        if (Valid::min($refer)->Validate($data->value) == false) {
                            $errors[] = "$data->label must be over $refer";
                        }
                        break;
                                              
                    case 'maxval':
                        if (Valid::max($refer)->Validate($data->value) == false) {
                            $errors[] = "$data->label must be under $refer";
                        }
                        break;

                    case 'email':
                        if (Valid::email()->Validate($data->value) == false) {
                            $errors[] = "$data->label must be a valid email";
                        }
                        break;
                        
                    case 'equal':
                        $equalTo = $load_var[$refer]->value;
                        if (Valid::equals($equalTo)->Validate($data->value) == false) {
                            $errors[] = "$data->label must be equal to ".$load_var[$refer]->label;
                        }
                        break;
                        
                    default:
                        $errors[] = "Rule not found: $rule";
                }
            }
        }
        return $errors;
    }
}
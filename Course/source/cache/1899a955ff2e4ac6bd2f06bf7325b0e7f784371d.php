<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $__env->yieldContent('browsertitle'); ?></title>

        <?php echo $__env->make('require/include_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
        <?php echo $__env->yieldContent('css'); ?>
    </head>
    <body>
        <br />
        <?php echo $__env->make('/segments/topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->yieldContent('outside'); ?>
            
        <div class="container">
            <?php echo $__env->yieldContent('content'); ?>
        </div>
        
        <?php echo $__env->make('segments/footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->make('require/include_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->yieldContent('bottomjs'); ?>

    </body>
</html>
@extends('frame')

@section('browsertitle')
    ACME: display
@stop

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h1> Users </h1>
            <hr />
            <table class="table table-stripe">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>E-mail</th>
                    <th>Age</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td> xx </td>
                        <td>{{ user->first_name }}</td>
                        <td>{{ user->last_name }}</td>
                        <td>{{ user->user_email }}</td>
                        <td>{{ user->user_age }}</td>
                    </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
@stop
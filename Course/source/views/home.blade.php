@extends('frame')

@section('browsertitle')
    A C M E
@stop

@section('outside')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="assets/slider/lighthouse.jpg" alt="lighthouse">
        </div>

        <div class="item">
          <img src="assets/slider/falls.jpg" alt="falls">
        </div>

        <div class="item">
          <img src="assets/slider/seagull.jpg" alt="seagull">
        </div>

        <div class="item">
          <img src="assets/slider/valley.jpg" alt="valley">
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    
    <div class="row">
        <div class="col-md-4 well text-center">
            <h3>About</h3>
            <span class="glyphicon glyphicon-globe big-icon" aria-hidden="true"></span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et massa risus. In ut accumsan elit. In viverra purus vitae mauris pellentesque auctor nec ac ante.</p>
        </div>
        <div class="col-md-4 well empty-well text-center">
            <h3>Tours</h3>
            <span class="glyphicon glyphicon-eye-open big-icon" aria-hidden="true"></span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et massa risus. In ut accumsan elit. In viverra purus vitae mauris pellentesque auctor nec ac ante.</p>
        </div>
        <div class="col-md-4 well text-center">
            <h3>Contact</h3>
            <span class="glyphicon glyphicon-earphone big-icon" aria-hidden="true"></span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et massa risus. In ut accumsan elit. In viverra purus vitae mauris pellentesque auctor nec ac ante.</p>
        </div>
    </div>
@stop

@section('bottomjs')
    <script>
        $('.carousel').carousel();
    </script>
@stop



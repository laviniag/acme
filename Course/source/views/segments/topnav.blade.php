<div id="topnav-bar">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="land">A C M E</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="home">Home</a></li>
            <li><a href="login">Login</a></li>
            <li><a href="about">About</a></li>
            <li><a href="register">Register</a></li>
            <li><a href="display">Display</a></li>
            <li><a href="entra">Entra</a></li>
            <li><a href="registra">Registra</a></li>
            <li><a href="tabella">Tabella</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
<br />
<br />
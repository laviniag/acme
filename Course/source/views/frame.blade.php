<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('browsertitle')</title>

        @include('require/include_css')
            
        @yield('css')
    </head>
    <body>
        <br />
        @include('/segments/topnav')

        @yield('outside')
            
        <div class="container">
            @yield('content')
        </div>
        
        @include('segments/footer')
        
        @include('require/include_js')
        
        @yield('bottomjs')

    </body>
</html>
<?php
namespace Acme\Controllers;

use duncan3dc\Laravel\BladeInstance;

class BaseController 
{      
    protected $loader;
    protected $twig;
    protected $blade;
    
    public function __construct ( )
    {
        $this->loader = new \Twig_Loader_Filesystem(__DIR__ . '/../views/');
        $this->twig = new \Twig_Environment($this->loader, ['cache'=>false,'debug'=>true]);    

        $this->blade = new BladeInstance(__DIR__ .'/../views/', __DIR__ .'/../cache/');
    }
}
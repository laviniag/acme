<?php
namespace Acme\Controllers;

use Acme\Models\Users;
use Acme\Validation\Validator;
use Illuminate\Database\Capsule\Manager as Capsule;

class SideController extends BaseController
{
    public function doLogin (array $params = null)
    {
        $load_var = [];
        $load_var['login_email'] = (object) [
            'value' => strtolower( htmlspecialchars(
                filter_input(INPUT_POST, 'login_email', FILTER_SANITIZE_EMAIL)  
            )),
            'label' => 'E-mail',
            'rules' => 'req|email',
            'store' => true
        ];
        $load_var['login_password'] = (object) [
            'value' => $_POST['login_password'],
            'label' => 'Password',
            'rules' => 'req|minlen:6|maxlen:32',
            'store' => true
        ];
        
        $valid = new Validator();
        $errors = $valid->isValid( $load_var );
        if (empty($errors)) {
            $login_email = $load_var['login_email']->value;
            $login_password = $load_var['login_password']->value;
            $pass = Users::select("user_pass")->where("user_email","=",$login_email)->get();
            if (sizeof($pass) > 0) {
                $password = $pass[0]->user_pass;
                if (password_verify($login_password, $password)) {
                    echo "<br> - Login accepted: $login_email <br>";
                } else {
                    echo "<br> - Login refused !!! <br>";
                }
            } else {   
                echo "<br> - User unknown !!! <br>";
            }
        } else {
            print_r($errors);
        }
    }

    public function doRegister (array $params = null)
    {
        $load_var = [];
        $load_var['first_name'] = (object) [
            'value' => ucwords( strtolower( htmlspecialchars(
                filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_STRING) 
            ))),
            'label' => 'First name',
            'rules' => 'req|minlen:3',
            'store' => true
        ];
        $load_var['last_name'] = (object) [
            'value' => ucwords( strtolower( htmlspecialchars(
                filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_STRING) 
            ))),
            'label' => 'Last name',
            'rules' => 'req|minlen:3',
            'store' => true
        ];
        $load_var['user_email'] = (object) [
            'value' => strtolower( htmlspecialchars(
                filter_input(INPUT_POST, 'user_email', FILTER_SANITIZE_EMAIL)  
            )),
            'label' => 'E-mail',
            'rules' => 'req|email',
            'store' => true
        ];
        $load_var['user_pass'] = (object) [
            'value' => $_POST['user_pass'],
            'label' => 'Password',
            'rules' => 'req|minlen:6|maxlen:32',
            'store' => true
        ];
        $load_var['user_verif'] = (object) [
            'value' => $_POST['user_verif'],
            'label' => 'Verifica Password',
            'rules' => 'req|equal:user_pass',
            'store' => false
        ];
        $load_var['user_age'] = (object) [
            'value' => (int) $_POST['user_age'],
            'label' => 'Età',
            'rules' => 'req|minval:18|maxval:100',
            'store' => true
        ];
        
        $valid = new Validator();
        $errors = $valid->isValid( $load_var );

        if (empty($errors)) {
            $load_var['user_pass']->value = password_hash($load_var['user_pass']->value, 
                                                          PASSWORD_DEFAULT);
            $user = new Users();
            foreach ($load_var as $prop => $load) {
                if ($load->store) {
                    $user->$prop = $load->value;
                }
            }
            $user->save();
            echo "<br>>>>> Data accepted !";
        } else {
            echo $this->twig->render('register.html', ['messages'=>$errors]);
            exit();
        }
    }
}
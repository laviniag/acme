<?php
namespace Acme\Controllers;

use Acme\Models\Users;

class PageController extends BaseController
{
    public function myHome (array $params = null)
    {
        // include (__DIR__ . '/../views/home.php');
        echo $this->twig->render('home.html');
    }
    
    public function blHome (array $params = null)
    {
        echo $this->blade->render("home");
    }
    
    public function myLogin (array $params = null)
    {
        // include (__DIR__ . '/../views/login.html');
        echo $this->twig->render('login.html');
    }    
    
    public function blLogin (array $params = null)
    {
        echo $this->blade->render("login");
    }
    
    public function myRegister (array $params = null)
    {
        // include (__DIR__ . '/../views/register.html');
        echo $this->twig->render('register.html');
    }
    
    public function blRegister (array $params = null)
    {
        echo $this->blade->render("register");
    }

    public function myDisplay (array $params = null)
    {
        $users = Users::select("*")->get();
        // include (__DIR__ . '/../views/display.php');
        echo $this->twig->render('display.html', ['users'=>$users]);
    }    
    
    public function blDisplay (array $params = null)
    {
        $users = Users::select("*")->get();
        echo $this->blade->render("display", ['users'=>$users]);
    }
    
    public function myAbout (array $params = null)
    {
        echo $this->twig->render('about.html');
    }
}
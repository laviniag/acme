<?php
namespace Acme\Controllers;
use Acme\Models\Routes;

class RouteController 
{      
    public function dispatch ( ) 
    {
        $router = new \AltoRouter();
        $router->setBasePath( getenv('BASE_PATH') );

        $routes = Routes::select('*')->get();
        foreach ($routes as $route) {
            $router->map( $route['route_url'], 
                          $route['route_ctrl'],
                          $route['route_meth'],
                          $route['route_vendor'],
                          $route['route_nspace'] );
        }

        $match = $router->match();
        if ( $match->found ) {
            if (is_callable(array($match->controller, $match->target))) {
                $controller = new $match->controller();
                call_user_func_array(array($controller, $match->target), array($match->params));
            }
        } else {
            echo "... Cannot find $match->missing <br>";
            exit();            
        } 
    }
}
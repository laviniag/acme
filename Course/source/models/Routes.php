<?php
namespace Acme\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Routes extends Eloquent {
    
    public $timestamps = false;
}
<?php

class SecureSession extends SessionHandler {

    private static $session_id;

    public function __construct( )
    {
        if ( ! isset(self::$session_id) ) {
            
            self::$session_id = session_id();

        	if( ! self::preventHijacking() )
        	{
        		$_SESSION = array();
        		$_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];
        		$_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                        echo "<br> Prevent <br>";
                $this->refresh();
        	}
                        echo " ID ----> ".session_id()." <br>";
        }
    }
    
    static private function preventHijacking()
    {
    	if(!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent']))
    		return false;

    	if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
    		return false;

    	if( $_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
    		return false;

    	return true;
    }

    public function forget()
    {
        if ( session_id() === '' ) {
            return false;
        }
        
        self::$session_id = null;
        $_SESSION = array();
        session_destroy();
        
        return new self();
    }

    public function refresh()
    {
        session_regenerate_id(false);
        session_regenerate_id(true);
    }
    
    public function get()
    {
        $arg_list = func_get_args();

        if ( func_num_args() == 1 ) {
            if ( isset($_SESSION[$arg_list[0]]) )
                return $_SESSION[$arg_list[0]];
            else 
                return null;
        }
        else {
            $result = array();
            foreach ( $arg_list as $argument ) {
                if ( isset($_SESSION[$argument]) )
                    $result[] = $_SESSION[$argument];
                else 
                    $result[] = null;
            }
            return $result;
        }
    }

    public function put( $key_name, $value )
    {
        if ( $key_name ) {
            $_SESSION[$key_name] = $value;
        }
        return $this;
    }
    
    public function reset()
    {
        $arg_list = func_get_args();

        if ( func_num_args() == 1 ) {
            if ( isset($_SESSION[$arg_list[0]]) )
                unset ($_SESSION[$arg_list[0]]);
        }
        else {
            $result = array();
            foreach ( $arg_list as $argument ) {
                if ( isset($_SESSION[$argument]) )
                    unset ($_SESSION[$argument]);
            }
        }
    }

}


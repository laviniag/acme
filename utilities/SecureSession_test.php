<?php

include "SecureSession.class.php";

session_start();
$session = new SecureSession();
var_dump($_SESSION);

list( $a, $b ) = $session->get('aaaa', 'dice');
echo "<br>". session_id()." >> ". $a." | ".$b."<br>";

var_dump($_SESSION);
$session = $session->forget();

list( $a, $b ) = $session->get('mice', 'dice');
echo "<br>". session_id()." >> ". $a." | ".$b."<br>";

var_dump($_SESSION);

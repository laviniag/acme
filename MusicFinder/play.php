<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Music Finder</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" 
		  http-equiv="Content-type" charset="ISO-8859-15" />
	<link rel="shortcut icon" href="images/music-note.ico" />
    <!-- Total Control HTML5 Audio Player -->
  	<link href="./jquery/style/jquery-ui.css" rel="stylesheet">
  	<link href="./totalcontrol/style/jquery.jscrollpane.css" rel="stylesheet">
    <!-- Bootstrap stylesheet -->
	<link href="./bootstrap/css/button_3D.css" rel="stylesheet" media="screen">   
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./bootstrap/css/login_form.css" rel="stylesheet" media="screen">
    <link href="./bootstrap/css/stylesheet.css" rel="stylesheet" media="screen">    
    <!-- jQuery (for Total Control HTML5 Audio Player) -->
    <script src="./jquery/jquery-1.6.4.min.js"></script>  
    <script src="./jquery/jquery-ui-1.11.2.min.js"></script>  
<script src="./totalcontrol/js/soundmanager/script/soundmanager2.js" type="text/javascript"></script>
<script src="./totalcontrol/js/jscrollpane/script/jquery.mousewheel.js" type="text/javascript"></script>
<script src="./totalcontrol/js/jscrollpane/script/jquery.jscrollpane.js" type="text/javascript"></script>
    <script src="./totalcontrol/totalcontrol.js"></script>
    <!-- Include all compiled bootstrap javascripts  -->
    <script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./mf_script/audioplay.js" type="text/javascript"></script> 
<?php
session_start();
include_once "fancybox/fancybox.html";
?>
</head>
<body>
<div class="container">
<script type='text/javascript'> initAudio(2); </script>
<form role="form" action="play.php" method="POST" name="play_page">
	
<?php
include_once "mf_bundle/constants.php";
include_once "mf_bundle/functions.php";
include_once "mf_bundle/buttons.php";
include_once "mf_bundle/noaccent.php";
include_once "mf_store/fetchplay.php";
include_once "mf_store/refersongs.php";
include_once "mf_store/plist_class.php";
include_once "mf_store/dbms.php";

// ------------ List contiene il nome della playlist --------------------------
if ( isset ($_REQUEST['list']) )
{
	init_playlist ( );
	$listname = $_REQUEST['list'];
	fetchPlay ( $listname );
	referSongs ( );
}
$listname = namePlay ( );	

// ----------------------------------------------------------------------------
$plist = new Plist ( );
if ( $plist->ply_total() > 0 )
{
	$plist->ply_getblock ( $trkind, $trkend );

//	echo ">>> $trkind | $trkend <br>";	
	$plist->ply_toppage ( $trkind );	
	$plist->ply_endpage ( $trkend );	
		
	play_buttons ( $listname );
?>	
	<div class="row">
	<div class="col-sm-4 col-md-4 col-md-push-8 to-stepdown">
		<div id="my-dialog" class="my-alert-bold"></div>
		<div id="my-loading" class="my-dialog-bold"></div>
		
		<div id="play-box" class="butt-hide">
			<ul id="play-sound" style="margin-left:auto; margin-right:auto;">
			</ul>
		</div>
		<div id="image-list" class="butt-show">
			<table class="tab-side">
<?php	
			if ( ! isset ($_REQUEST['list']) && $plist->ply_current() > 0 ) {
				echo "<script type='text/javascript'> musicPlayer(); </script>";
			} else {
				$imgarr = $plist->ply_getimages ( );
				showImages ( $imgarr );
			}
?>
			</table>
		</div>
	</div>
		
	<div class="col-sm-8 col-md-8 col-md-pull-4" style="display: inline-block">	
	<ul>
<?php
	for ( $ind = $trkind; $ind <= $trkend && $plist->ply_actual($ind) != END_PAGE; $ind++ ) 
	{	
		$idaudio = "au" . $ind;
		$imgname = "im" . $ind;			
		$idbox = "ck" . $ind;
		$flag = ( $plist->ply_ischecked($ind) ) ? "checked" : "";					
		
		$display = titleArtist ( $plist->title );
?>  	
        <li class="high-line">
        <div class="checkbox">
   			<input type="checkbox" id="<?= $idbox ?>" <?= $flag ?> name="chkarr[<?= $ind ?>]" value=1>
           	<p class="to-shadow" onclick="showTrack(<?= $ind ?>)">
           		<?= $plist->title ?>
           		<img id="<?= $imgname ?>" class="to-endline" src="images/button_play.png">
			</p>
        	<div class="to-right" style="display: none">
    	  		<ul id="<?= $idaudio ?>" style="list-style-type: none;">
 					<li mp3="<?= $plist->song ?>" artist="<?= $display[0] ?>" 
					title="<?= $display[1] ?>" artwork=""></li>
				</ul>			
			</div>			
		</div>
		</li>				
<?php
	}
	unset ($plist);	
?>
	</ul>
	</div>
	</div>
	<div id="box-start" class="butt-hide"><?= $trkind ?></div>
	<div id="box-stend" class="butt-hide"><?= $trkend ?></div>
<?php
}
?>
</form>
</div>
</body>
</html>
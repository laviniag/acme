// Funzioni di memorizzazione playlist nel Database
var prev_play = 0;
var prev_list = "";

function savePlist( option ) {
	var idplay = 0;
	var listname = "";
	if ( option == 1 ) {
		listname = codeCharSet (document.getElementById("InputPlay").value);
		if ( listname.indexOf("\<") >= 0 ) {
			document.getElementById("alert-1").innerHTML = "Caratteri non ammessi";
			return;
		}
		if ( listname == "" ) {
			document.getElementById("alert-1").innerHTML = "Inserisci nome playlist";
			return;
		}
		if ( listname == prev_list )
			return;
		else
			prev_list = listname;
	} else {
		var howmany = document.save_play.id_play.length;
		for (var ind=0; ind < howmany; ind++) {
			if ( document.save_play.id_play[ind].checked ) {
				idplay = document.save_play.id_play[ind].value;
				break;
			}
		}
		if ( idplay == 0 ) {
			document.getElementById("alert-2").innerHTML = "Seleziona una playlist";
			return;
		}
		if ( idplay == prev_play )
			return;
		else
			prev_play = idplay;
	}
	var xmlhttp = new XMLHttpRequest();
	var text = "../mf_store/storeplay.php?play=" + idplay + "&name=" + listname + "&alt";
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {

		}
	}
	xmlhttp.send();

	var confirm = document.getElementById("list-ok");
	confirm.setAttribute("class", "butt-show");
};

function plistControl( option ) {
	var idplay = 0;
	var count = 0;
	var plyarr = new Array();

	var howmany = document.merge_play.plyarr.length;
	for (var ind=0; ind < howmany; ind++) {
		if ( document.merge_play.plyarr[ind].checked ) {
			idplay = document.merge_play.plyarr[ind].value;
			if ( option == 2 ) var renind = ind;
			plyarr.push(idplay);
			count ++;
		}
	}
	if ( option == 1 && count < 2 ) {
		document.getElementById("alert-box").innerHTML = "Seleziona almeno 2 playlist";
		return;
	}
	if ( count == 0 ) {
		document.getElementById("alert-box").innerHTML = "Seleziona almeno 1 playlist";
		return;
	}
	if ( option == 2 && count > 1 ) {
		document.getElementById("alert-box").innerHTML = "Seleziona 1 sola playlist";
		return;
	}
	if ( option == 1 || option == 2 ) {
		var listname = document.getElementById("InputPlay").value;
		if ( listname.indexOf("\<") >= 0 ) {
			document.getElementById("alert-box").innerHTML = "Caratteri non ammessi";
			return;
		}
		if ( listname == "" ) {
			document.getElementById("alert-box").innerHTML = "Inserisci nome playlist";
			return;
		}
		if ( option == 2 ) {
			var idrename = "id" + renind;
			document.getElementById(idrename).innerHTML = listname;
		}
		listname = codeCharSet (listname);
	}

	var querystring = JSON.stringify(plyarr);
	var xmlhttp = new XMLHttpRequest();
	if ( option == 1 )
		var text = "../mf_store/storechange.php?merge=" + querystring + "&name=" + listname;
	else if ( option == 2 )
		var text = "../mf_store/storechange.php?rename=" + plyarr[0] + "&name=" + listname;
	else if ( option == 3 )
		var text = "../mf_store/storechange.php?delete=" + querystring;

	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {

		}
	}
	xmlhttp.send();

	var confirm = document.getElementById("id-exit");
	var dialog = document.getElementById("dialog-box");
	if ( option == 1 ) {
		confirm.setAttribute("class", "btn btn-fresh");
		dialog.innerHTML = "Per accedere alla nuova playlist <br>passa dal menu principale"
	} else if ( option == 2 ) {
		confirm.setAttribute("class", "btn btn-light");
	} else if ( option == 3 ) {
		confirm.setAttribute("class", "btn btn-sky");
	}
	document.getElementById("list-ok").setAttribute("class", "butt-show");;
};

function codeCharSet( str ) {
	str = str.replace(/\+/g, "\x18");
	str = str.replace(/\=/g, "\x19");
	str = str.replace(/\&/g, "\x1C");
	str = str.replace(/\#/g, "\x1D");

	return str;
};
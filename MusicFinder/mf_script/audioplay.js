//		Global variables and constant
var prev_div = "";
var prev_ul = "";
var prev_li = "";
var prev_id = 0;
var play_list = 0;
var music_on = false;

function showTrack( trkind ) {
	if ( prev_id == 9999 )			//  Disattivato se sta suonando la playlist
  		return;
		
	document.getElementById("my-dialog").innerHTML = "";  	
    if ( prev_id != 0 ) {
    	$('#total-playlist').stopControl();
    	var previmg = "im" + prev_id;
    	document.getElementById(previmg).src = "images/button_play.png";
    	
		prev_ul.setAttribute("id", "au"+prev_id); 	
		while ( prev_ul.firstChild ) {
    		prev_ul.removeChild(prev_ul.firstChild);
		}
		prev_ul.removeAttribute("class");
		prev_ul.appendChild(prev_li);
    	prev_div.style.display = "none";		
	}
    if ( prev_id == trkind ) {
 		initAudio( 0 );
    } else { 
	   	var idaudio = "au" + trkind;
    	var	ulnode = document.getElementById(idaudio); 
       	var divnode = ulnode.parentNode;
    	var lispace = ulnode.getElementsByTagName("li");

        var idimage = "im" + trkind;	
    	document.getElementById(idimage).src = "images/button_stop.png";    	    	     	
    	ulnode.setAttribute("id", "total-playlist");
    	prev_li = lispace[0];
    	divnode.style.display = "block";
    	prev_div = divnode;
    	prev_ul = ulnode;
    	prev_id = trkind;
    	
		$('#total-playlist').totalControl({
			style:   'default.css',
		    position: 'relative',
		    shuffleEnabled: false,
		    repeatOneEnabled: true,
		    repeatAllEnabled: false,
		    playlistVisible: false,
		    autoplayEnabled: true,
		    playlistHeight: 100,
		    playlistSortable: false,
		    checkboxesEnabled: false,
		    songTooltipEnabled: false,
		    songTooltipPosition: "bottom",
		    miniPlayer: false,
		    isDraggable: false,
		    addSongEnabled: false
		});	
	}
};

function initAudio( option ) {
	if ( option > 0 )
		play_list = option;
	music_on = false;
	prev_div = "";
	prev_ul = "";    	
	prev_li = "";    	
	prev_id = 0;
};

function checkSelectAll( onoff ) {
	document.getElementById("my-dialog").innerHTML = "";
	var start = parseInt (document.getElementById("box-start").innerHTML);
	var stend = parseInt (document.getElementById("box-stend").innerHTML);
// alert ("start= " + start + " ! end= " + stend);
	var idbox = "ck" + start;
	for (var ind = start; ind <= stend; ind++) {
		idbox = "ck" + ind;
		if ( onoff == 1 )
			document.getElementById(idbox).checked = true;
		else
			document.getElementById(idbox).checked = false;
	}
};	

function buttonControl( dest ) {
	var start = parseInt (document.getElementById("box-start").innerHTML);
	var stend = parseInt (document.getElementById("box-stend").innerHTML);
	var dialog = document.getElementById("my-dialog");
   	dialog.innerHTML = "";

	var chkarr = new Array();
	chkarr.push (0);
	var idbox = "ck" + start;
	for (var ind = start; ind <= stend; ind++) {
		idbox = "ck" + ind;
		if ( document.getElementById(idbox).checked )
			chkarr.push(ind);
		else
			chkarr.push(-ind);
	}
	var querystring = JSON.stringify(chkarr);
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_bundle/settings.php?func=" + querystring;
		
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
		  switch ( dest ) {
			case 1:									//  Menu principale
				window.location="index.php?menu";
				break;
			case 2:									//  Pagina precedente
				window.location="music.php?back";
				break;
			case 3:									//  Pagina successiva
				window.location="music.php?forw";
				break;	
				
			case 4:									//  Ascolta playlist
				if ( music_on && play_list == 1 ) break;
			case 5:									//  Salva playlist
				var select = parseInt (trimQueue (xmlhttp.responseText));
    			if ( select == 0 ) {
    				dialog.innerHTML = "SELEZIONA PRIMA LE CANZONI";
    				parent.jQuery.fancybox.close();
    				break;
				}
				if ( dest == 5 ) {					//  Salva playlist: deseleziona la pagina
					checkSelectAll ( false );
				} 
				else if ( dest == 4 ) {
					musicPlayer( );
				} 
				break;
				
			case 6:									//  Salva playlist / utente non loggato
    			dialog.innerHTML = "EFFETTUARE LOGIN O REGISTRAZIONE";
				break;	
				
			case 7:									//  Aggiorna playlist
    			window.play_page.submit();
				break;
				
			case 8:									//  Modifica playlist
				break;
		  }
		} 
	}
	xmlhttp.send();
};

function musicPlayer( ) {
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_bundle/settings.php?play";
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
			var resp = trimQueue (xmlhttp.responseText);
//	alert (resp);
			var list = JSON.parse (resp);
			if ( list[0].mp3 == "" && list[0].artist == "" ) {
				var dialog = document.getElementById("my-dialog");
				dialog.innerHTML = "NESSUNA CANZONE SELEZIONATA";
				parent.jQuery.fancybox.close();
				return;
			}
			if ( prev_id > 0 && prev_id < 9999 ) 
				showTrack( prev_id );
			prev_id = 9999;
			
			var newly = document.getElementById("image-list");
			newly.setAttribute("class", "butt-hide");
			var arrows = document.getElementsByClassName("to-endline");
			for (var ind=0; ind < arrows.length; ind++)
  				arrows[ind].style.display = "none";
			
			if ( play_list == 2 ) {
				if ( document.getElementById("up-play") ) {
					var update = document.getElementById("up-play");
					update.setAttribute("class", "butt-hide");
					var update = document.getElementById("up-date");
					update.setAttribute("class", "btn btn-light");
				}
			}
			var playbox = document.getElementById("play-box");
			playbox.setAttribute("class", "butt-show");      				
			var player = document.getElementById("play-sound");
			player.setAttribute("id", "total-playlist"); 
			music_on = true;						//  Attiva flag RIPRODUZIONE IN CORSO
			
			for (var ind = 0; ind < list.length; ind++) {
				newly = document.createElement("li");	
				newly.setAttribute("mp3", list[ind].mp3);
				newly.setAttribute("artist", list[ind].artist);
				newly.setAttribute("title", list[ind].title);
				newly.setAttribute("artwork", list[ind].artwork);
				player.appendChild (newly);
			}    			
		
			$('#total-playlist').totalControl({
				style:   'default.css',
			    position: 'relative',
			    shuffleEnabled: true,
			    repeatOneEnabled: true,
			    repeatAllEnabled: true,
			    playlistVisible: true,
			    autoplayEnabled: true,
			    playlistHeight: 360,
			    playlistSortable: true,
			    checkboxesEnabled: true,
			    songTooltipEnabled: true,
			    songTooltipPosition: "bottom",
			    miniPlayer: false,
			    isDraggable: false,
			    addSongEnabled: false
			});
		}
	}
	xmlhttp.send();	
};

function trimQueue (xmlstring) {
	var ind = xmlstring.indexOf ("<");
	if ( ind > -1 )
		return xmlstring.substring( 0, ind );
	else
		return xmlstring;
};
// Funzioni di input o setting del menu principale
function inputControl( reg ) {
	var user = document.getElementById("email").value;
	var passw = document.getElementById("password").value;
	
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_store/loginform.php?user=" + user + "&pass=" + passw + "&reg=" + reg;
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			xmlstring = trimQueue (xmlhttp.responseText);
			user = parseInt (xmlstring);
//			alert (user);
			if ( user > 0 && user < 9999 ) {
				document.getElementById("play-list").setAttribute("class","butt-show");
				document.getElementById("log-out").setAttribute("class","butt-show");
				document.getElementById("log-in").setAttribute("class","butt-hide");
				document.getElementById("login-alert").innerHTML = "";
				if ( reg == 1 ) {
					yourPlaylist( user );
				}
			}
			else {
				document.getElementById("login-alert").innerHTML = xmlstring;
			}
		}
	}
	xmlhttp.send();
};

function inputLogout( ) {
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_store/loginform.php?logout";
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var resp = trimQueue (xmlhttp.responseText);
//			alert(resp);
			if ( resp == "OK" ) {
				var newly = document.getElementById("list-name");
				while (newly.firstChild) {
    				newly.removeChild(newly.firstChild);
				}
				document.getElementById("play-list").setAttribute("class","butt-hide");
				document.getElementById("log-out").setAttribute("class","butt-hide");
				document.getElementById("log-in").setAttribute("class","butt-show");
			}
		}
	}
	xmlhttp.send();
};

function yourPlaylist( user ) {
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_store/insertplay.php?login=" + user;
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var player = document.getElementById("list-name");
			xmlstring = trimComments (xmlhttp.responseText);
//		alert (xmlstring);
			if (window.DOMParser) {
  				var parser = new DOMParser();
  				var list = parser.parseFromString(xmlstring,"text/xml");
  			} else { 		// Internet Explorer
				var list = new ActiveXObject("Microsoft.XMLDOM");
  				list.async = false;
				list.loadXML (xmlstring);
  			} 
  			var newly = "";		var ahref = "";
  			var xind = list.getElementsByTagName("ind");
			for ( var ind=0; ind < xind.length; ind++ ) {
				newly = document.createElement("li");			 
				ahref = document.createElement("a");
				text = xind[ind].getElementsByTagName("href")[0].childNodes[0].nodeValue;	 
				ahref.setAttribute("href", "play.php?list=" + text);
				ahref.innerHTML = xind[ind].getElementsByTagName("name")[0].childNodes[0].nodeValue; 
				player.appendChild (newly);
				newly.appendChild (ahref);
			}
		}
	}
	xmlhttp.send();
};

function loadFeatures( ) {
	if ( document.getElementById("my-warning") )
		document.getElementById("my-warning").setAttribute("class","butt-hide");
								
	var xmlhttp = new XMLHttpRequest();
	var text = "mf_bundle/features.php";
	xmlhttp.open ("POST", text, true);
	xmlhttp.onreadystatechange = function() {
		if( xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			xmlstring = trimComments (xmlhttp.responseText);
//	alert (xmlstring); 
			if (window.DOMParser) {
  				var parser = new DOMParser();
  				var list = parser.parseFromString(xmlstring,"text/xml");
  			} else { 		// Internet Explorer
				var list = new ActiveXObject("Microsoft.XMLDOM");
  				list.async = false;
				list.loadXML (xmlstring);
  			} 
  			var feat = list.getElementsByTagName("feat");
  			if ( feat.length > 0 )
  				document.getElementById("feat-head").setAttribute("class","butt-show");

			var artist = "";  	var image = "";	
			var idtag = "";		var tagelem = "";
			for ( var ind=0; ind < feat.length; ind++ ) {
				artist = feat[ind].getElementsByTagName("artist")[0].childNodes[0].nodeValue;	 
				image = feat[ind].getElementsByTagName("image")[0].childNodes[0].nodeValue; 
				idtag = "bt" + ind;
				tagelem = document.getElementById(idtag);
				tagelem.setAttribute("value", artist);
				tagelem.setAttribute("class", "butt-show");
				idtag = "img" + ind;
				tagelem = document.getElementById(idtag);				
				tagelem.setAttribute("src", image);
				idtag = "sp" + ind;
				tagelem = document.getElementById(idtag);				
				tagelem.innerHTML = artist;
			}
		}
	}
	xmlhttp.send();
};

function trimComments (xmlstring) {
	var output = "<?xml version='1.0' encoding='UTF-8'?>";
	
	var ind = xmlstring.indexOf ("<mf_list>");
	var jnd = xmlstring.indexOf ("</mf_list>");

	if ( ind > -1 )
		output += xmlstring.substring( ind, jnd + 10 );
 	else
 		output += "";

  	return output;
};

function trimQueue (xmlstring) {
	var ind = xmlstring.indexOf ("<");
	if ( ind > -1 )
		return xmlstring.substring( 0, ind );
	else
		return xmlstring;
};
<?php
function disp_buttons ( $topdisp, $enddisp, $offlimit )
{
?>
<nav class="navbar navbar-default well text-center" role="navigation">
<br>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    	<div class="row to-upper">

            <button type="button" class="btn btn-fresh" onclick="buttonControl(1)">
            	<i class="glyphicon glyphicon-align-justify"></i>
            	<strong>&nbsp MENU &nbsp</strong>
            </button>
            		
            <button type="button" class="btn btn-sunny" onclick="checkSelectAll(1)">
            	<strong>SELEZIONA TUTTO</strong>
            </button> 
            <button type="button" class="btn btn-sky" onclick="checkSelectAll(0)"> 
            	<strong>TOGLI SELEZIONE</strong>
            </button>  
     
            <button type="button" class="btn btn-hot" onclick="buttonControl(4)">
            	<strong>ASCOLTA SELEZIONE</strong>
            </button> 
<?php  
            if ( userLogin () > 0 ) {
?>            		
            	<a class="isquare" href="mf_store/saveplay.php" onclick="buttonControl(5)"
            	data-fancybox-type="iframe">
            		<button type="button" class="btn btn-sunny">
            			<strong>SALVA SELEZIONE</strong>
            		</button>
            	</a>
<?php
            } else {
?>
            	<button type="button" class="btn btn-pale" onclick="buttonControl(6)">
            		<strong>SALVA SELEZIONE</strong>
            	</button>
<?php           
			} 
?> 
        	<ul class="nav navbar-nav navbar-right">	
<?php		
            if ( $topdisp > 1 ) {  
?>                
				<li>
            	<button type="button" class="btn btn-fresh" onclick="buttonControl(2)">
            		<i class="glyphicon glyphicon-backward"></i>
            		<strong>&nbsp PRECEDENTE</strong>
            	</button> 
	            </li>           		
<?php
            } else {
?>
				<li>
            	<button type="button" class="btn btn-gray">
            		<i class="glyphicon glyphicon-backward"></i>
            		<strong>&nbsp PRECEDENTE</strong>
            	</button> 
	            </li> 
<?php           
			} 
            if ( $topdisp < $enddisp && ($enddisp < $offlimit 
            || ($enddisp-$topdisp+1) % loadLimit() == 0) ) { 
?>             	
				<li>
            	<button type="button" class="btn btn-fresh" onclick="buttonControl(3)">
            		<i class="glyphicon glyphicon-forward"></i>
            		<strong> SUCCESSIVO</strong>
            	</button>
	            </li>
<?php
            } else {
?>
				<li>
            	<button type="button" class="btn btn-gray">
            		<i class="glyphicon glyphicon-forward"></i>
            		<strong> SUCCESSIVO</strong>
            	</button>
	            </li>
<?php            
            }           	
?>
			</ul>
    	</div>
    </div> <!--/.navbar-collapse -->
</nav>
<?php
}

function play_buttons ( $listname )
{
	$listname = metaCharSet ( $listname );
?>
<nav class="navbar navbar-default well text-center" role="navigation">
    <br>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    	<div class="row to-upper">

            <button type="button" class="btn btn-fresh" onclick="buttonControl(1)">
            	<i class="glyphicon glyphicon-align-justify"></i>
            	<strong>&nbsp MENU &nbsp</strong>
            </button>
            		
            <button type="button" class="btn btn-sunny" onclick="checkSelectAll(1)">
            	<strong>SELEZIONA TUTTO</strong>
            </button> 
            <button type="button" class="btn btn-sky" onclick="checkSelectAll(0)">
            	<strong>TOGLI SELEZIONE</strong>
            </button>  
     
            <button type="button" id="up-play" class="btn btn-hot" onclick="buttonControl(4)">
            	<strong>ASCOLTA PLAYLIST</strong>
            </button>
            <button type="button" id="up-date" class="butt-hide" onclick="buttonControl(7)">
            	<strong>AGGIORNA PLAYLIST</strong>
            </button>
            
        	<a class="isquare" href="mf_store/mergeplay.php" onclick="buttonControl(8)"
        	data-fancybox-type="iframe">
        	    <button type="button" class="btn btn-sunny">
        			<strong>UNISCI / RINOMINA</strong>
        		</button>
        	</a>

        	<ul class="nav navbar-nav navbar-right">	
			    <div id="list-name" class="navbar-header">
			        <a class="navbar-brand" href="#"><?= $listname ?></a>
			    </div>
			</ul>
        </div>
    </div> <!--/.navbar-collapse -->
</nav>
<?php
}
?>
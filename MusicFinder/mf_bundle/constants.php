<?php
define ("OK_CODE", 200);              	//  Codice di operazione riuscita
define ("SET_START", 1);               	//  Codice di inizio pagina
define ("END_PAGE", -1);               	//  Codice della fine pagina
define ("GET_PAGE", -1);               	//  Codice di return pagina
define ("SET_RELOAD", 9);             	//  Codice di riavvio pagina
define ("ROLL_BACK", 99);             	//  Codice di riavvio pagina
define ("PAGE_LIMIT", 20);              //  Limite tracce per pagina
define ("FEAT_LIMIT", 20);              //  Limite artisti in evidenza
define ("DEF_ZERO", 0);               	//  Codice generico di default
define ("LOG_OUT", 0);               	//  Codice di logout utente
define ("CHK_UNDEF", -2);              	//  Flag del check = undefined
define ("CHK_INACT", 0);               	//  Flag del check = inattivo
define ("CHK_ACTIVE", 2);     			//  Flag del check = attivo
define ("FORWARD", 11);     			//  Codice per indicare AVANTI
define ("BACKWARD", 12);     			//  Codice per indicare INDIETRO
define ("GET_USER", -3);     			//  Codice per ottenere username
define ("NEW_USER", 2);     			//  Codice nuovo utente
define ("PLAY_LIST", 10);     			//  Codice display playlist
?>
<?php
/*
    Classe TRACK: gestisce le ricerche tramite arrays della SESSION
*/
class Track
{
    //----- Inizializzazioni mediante costruttore -----------------------
    function __construct()
    {
        $this->check = false;
        $this->title = "";
        $this->save = "";
        $this->song = "";
    }

//------- Inserisce una nuova traccia in Sessione -----------------------
    function trk_puts ( $title, $save, $song )
    {
   		$index = $_SESSION['track_count'] + 1;
   		$_SESSION['track_count'] = $index;
 	    $_SESSION['track_check'][$index] = false;
 	    $_SESSION['track_title'][$index] = $title;
 	    $_SESSION['track_save'][$index] = $save;
 	    $_SESSION['track_song'][$index] = $song;
    }

//------- Fetch della traccia corrente ----------------------------------
    function trk_actual ( $trkind )
    {
    	if ( $_SESSION['track_count'] >= $trkind )
    	{
        	$this->check = $_SESSION['track_check'][$trkind];
	        $this->title = $_SESSION['track_title'][$trkind];
	        $this->save = $_SESSION['track_save'][$trkind];
	        $this->song = "http://ololo.fm/download/". $_SESSION['track_song'][$trkind];
        	return OK_CODE;
	  	} else
	  		return END_PAGE;
    }

//------- Setta il nome dell'artista cercato ----------------------------
    function trk_singer ( $singer="" )
    {
    	if ( $singer == "" )
    		return $_SESSION['track_singer'];
    	else
 	    	$_SESSION['track_singer'] = $singer;
    }

//------- Riassegna il campo url della canzone --------------------------
    function trk_assign ( $trkind, $song )
    {
 	    $_SESSION['track_song'][$trkind] = $song;
    }

//------- Toggle dello stato della traccia on/off -----------------------
    function trk_check ( $trkind, $onoff )
    {
   		$_SESSION['track_check'][$trkind] = $onoff;
    }

//------- Restituisce lo stato del checkbox on/off ---------------------
    function trk_ischecked ( $trkind )
    {
    	return $_SESSION['track_check'][$trkind];
    }

//------- Restituisce il numero complessivo di tracce -------------------
    function trk_total ( )
    {
    	$total = $_SESSION['track_count'];
   		return $total;
    }

//------- Mette il puntatore di inizio/fine pagina ----------------------
    function trk_setblock ( $flag )
    {
    	if ( $flag == SET_START ) {
    		$_SESSION['track_count'] = $_SESSION['page_max'];
    		$_SESSION['block_top'] = $_SESSION['track_count'] + 1;
	    } else {
	    	$_SESSION['block_end'] = $_SESSION['track_count'];
    	}
    }

//------- Restituisce puntatore di inizio/fine pagina -------------------
    function trk_getblock ( &$block1, &$block2 )
    {
    	if ( $_SESSION['block_top'] > 0 )
    	{
	  		$block1 = $_SESSION['block_top'];
	  		$block2 = $_SESSION['block_end'];
	  		return true;
	  	} else
	  		return false;
    }

//------- Setta la nuova pagina da cercare sul web ----------------------
    function trk_newpage ( $wpage=GET_PAGE )
    {
    	if ( $wpage == GET_PAGE )
    	{
    		$npage = $_SESSION['track_lastpage'] + 1;
    		$_SESSION['track_lastpage'] = $npage;
  			return $npage;
  		} else
  			$_SESSION['track_lastpage'] = $wpage;
    }

//------- Setta punto d'inizio della pagina visualizzata ----------------
    function trk_toppage ( $wcount=GET_PAGE )
    {
    	if ( $wcount == GET_PAGE )
    		return $_SESSION['page_top'];
    	else {
  			$_SESSION['page_top'] = $wcount;
    	}
    }

//------- Setta punto finale della pagina visualizzata ------------------
    function trk_endpage ( $wcount=GET_PAGE )
    {
    	if ( $wcount == GET_PAGE )
    		return $_SESSION['page_end'];
    	else {
    		if ( $wcount > $_SESSION['page_max'] )
    		    $_SESSION['page_max'] = $wcount;
  			$_SESSION['page_end'] = $wcount;
    	}
    }

//------- Memorizza l'immagine dell'artista -----------------------------
    function trk_setimage ( $singer, $image )
    {
   		$imgind = $_SESSION['block_top'];
    	$_SESSION['block_artist'][$imgind] = $singer;
    	$_SESSION['block_image'][$imgind] = $image;
    }


//------- Cerca l'immagine associata a una canzone ----------------------
    function trk_findimage ( $idsong, &$artist, &$image )
    {
       	$imagecopy = $_SESSION['block_image'];
   		$imagecopy[$_SESSION['track_count']+1] = "";
   		$indstore = 0;

   		foreach ( $imagecopy as $indblock => $imgblock )
    	{
    		if ( $indstore <= $idsong && $idsong < $indblock ) {
				$artist = $_SESSION['block_artist'][$indstore];
				$image = $_SESSION['block_image'][$indstore];
				break;
    		}
			$indstore = $indblock;
    	}
    }

//------- Restituisce array con immagini degli artisti ------------------
    function trk_getimages ( )
    {
    	$imgarr = array ();
    	if ( empty ($_SESSION['block_image']) )
    		return $imgarr;

    	$imgind = 0;
    	$trkind = $_SESSION['page_top'];
    	$trkend = $_SESSION['page_end'];
    	$indstore = 1;
    	$imagecopy = $_SESSION['block_image'];
    	$imgstore = $imagecopy[1];
   		$imagecopy[$_SESSION['track_count']+1] = "";

   		foreach ( $imagecopy as $indblock => $imgblock )
    	{
    		if ( $trkind == $indblock ) {
     		    $imgarr[$imgind++] = $imgblock;
    		} elseif ( $trkind >= $indstore && $trkind < $indblock ) {
    			if ( $imgind == 0 || $imgarr[$imgind-1] != $imgstore )
    		    	$imgarr[$imgind++] = $imgstore;
    		} elseif ( $trkend >= $indstore && $trkend < $indblock ) {
    	   		if ( $imgind == 0 || $imgarr[$imgind-1] != $imgstore )
    	   			$imgarr[$imgind++] = $imgstore;
    		} elseif ( $trkind < $indstore && $trkend > $indblock ) {
    		 	$imgarr[$imgind++] = $imgstore;
    		}
    		$indstore = $indblock;
    		$imgstore = $imgblock;
    	}
    	return $imgarr;
    }
}
?>
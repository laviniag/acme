<?php
session_start();
//	--------------- Carica i link delle canzoni ad uso Playlist ---------------
include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/track_class.php";
include_once "../mf_store/plist_class.php";

if ( isset ($_REQUEST['start']) )
{
	$plist = new Plist ( ); 
	$plyind = 1;
	$plyend	= $plist->ply_total( );	   

	while ( $plyind <= $plyend )
	{
		$plist->ply_actual ( $plyind );
		if ( $plist->song == "" )
		{		
			$trials = 0;
			while ( ($urlsong = getUrlSong($plist->save)) == "" && $trials < 4 )
				$trials ++;
			
			if ( $urlsong != "" )
				$plist->ply_assign ( $plyind, $urlsong );
		}
		$plyind ++;
	}
	unset ( $plist );
}
elseif ( isset ($_REQUEST['trk']) )
{
	$trkind = $_REQUEST['trk'];
	$track = new Track ( ); 		   
	$track->trk_actual ( $trkind );

	if ( $track->song == "" )
	{		
		$trials = 0;
		while ( ($urlsong = getUrlSong($track->save)) == "" && $trials < 4 )
			$trials ++;
		
		if ( $urlsong != "" )
			$track->trk_assign ( $trkind, $urlsong );
	}
	unset ( $track );
	
	echo $urlsong; 					//  risposta per javascript
}
elseif ( isset ($_REQUEST['ply']) )
{
	$plyind = $_REQUEST['ply'];
	$plist = new Plist ( ); 		   
	$plist->ply_actual ( $plyind );

	if ( $plist->song == "" )
	{		
		$trials = 0;
		while ( ($urlsong = getUrlSong($plist->save)) == "" && $trials < 4 )
			$trials ++;
		
		if ( $urlsong != "" )
			$plist->ply_assign ( $plyind, $urlsong );
	}
	unset ( $plist );
	
	echo $urlsong; 					//  risposta per javascript
}

//	------------ Restituisce l'url della canzone da ascoltare -----------------
function getUrlSong ( $songref )
{
    $urlsearch = "http://ololo.fm/$songref";
	$strweb = file_get_contents($urlsearch); 
		
    $start = strpos( $strweb, "href=\"/download/", 100);
    if ( $start !== false ) 
    {
	    $start += 7;
		$stend = strpos ( $strweb, ".mp3\"", $start);
		$stend += 4;
		$urlsong = "http://ololo.fm/" . substr ($strweb, $start, $stend-$start);
		return $urlsong;
	}
	else
		return "";
}
?>
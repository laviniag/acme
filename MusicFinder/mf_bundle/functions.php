<?php
//------- Inizializza gli array di ricerca in Sessione ------------------------
function init_session ( )
{
	$_SESSION["init_plist"] = false;
	if ( isset ($_SESSION["init_done"]) && $_SESSION["init_done"] )
		return;

	$_SESSION['track_count'] = 0;
	$_SESSION['track_check'][] = false; 
	$_SESSION['track_title'][] = "";
	$_SESSION['track_save'][] = "";   
	$_SESSION['track_song'][] = "";   
	$_SESSION['track_singer'] = "";  
	$_SESSION['track_lastpage'] = 0; 
	$_SESSION['page_top'] = 1;	 	    
	$_SESSION['page_end'] = 1;
	$_SESSION['page_max'] = 0;
	$_SESSION['load_limit'] = PAGE_LIMIT;
	$_SESSION["load_skip"] = "00";	
	$_SESSION["user_login"] = LOG_OUT; 
	$_SESSION["init_done"] = true; 	    
}

//------- Inizializza gli array di playlist in Sessione -----------------------
function init_playlist ( )
{
	unset ( $_SESSION['plist_check'] ); 
	unset ( $_SESSION['plist_idart'] );
	unset ( $_SESSION['plist_title'] );
	unset ( $_SESSION['plist_song'] );
	unset ( $_SESSION['plist_artist'] );
	unset ( $_SESSION['plist_image'] );

	$_SESSION['plist_count'] = 0;
	$_SESSION['plist_check'][] = false; 
	$_SESSION['plist_idart'][] = 0; 
	$_SESSION['plist_title'][] = "";
	$_SESSION['plist_song'][] = "";   
	$_SESSION['plist_current'] = 0;  
	$_SESSION['plist_lastpage'] = 0; 
	$_SESSION['plist_top'] = 1;	 	    
	$_SESSION['plist_end'] = 1;
	$_SESSION['pagelist_top'] = 1;	 	    
	$_SESSION['pagelist_end'] = 1;
	$_SESSION["init_plist"] = true;	
}

//------- Estrae nome artista + titolo e li normalizza ------------------------
function titleArtist ( $title )
{
	$tmptitle = explode ("[", $title);
	$display = explode ("-", $tmptitle[0]);
	if ( sizeof ($display) == 1 )
		$display[1] = $display[0];
	elseif ( sizeof ($display) == 3 )
		$display[1] .= $display[2];
		
	return $display;	
}

//------- Ricava titolo completo e scarta titoli non adeguati -----------------
function titleFilter ( $title )
{
	$tend = strpos ( $title, "[", 0 );				//  toglie contenuti in [...]
	if ( $tend !== false )	
		$title = substr ( $title, 0, --$tend );
	else
		$tend = strlen ( $title );

	if ( preg_match ('/[А-Яа-яЁё]/', $title) )
		return false;
	if ( ! strpos ( $title, "-", 0 ) )
		return false;
	$hyphen = explode ( "-", $title );
	if ( ! preg_match ('/[A-Za-z0-9]/', $hyphen[0])
	||   ! preg_match ('/[A-Za-z0-9]/', $hyphen[1]) )
		return false;
		
	if ( $tend > 76 ) {
		for ( $ind = 76; $ind > 0; $ind -- )
			if ( $title[$ind] == " " ) break;
		$title[$ind] = "\x1D";
		$title = str_replace ("\x1D", "<br> ", $title);
	}	
	if ( $tend > 156 ) {
		for ( $ind = 156; $ind > 0; $ind -- )
			if ( $title[$ind] == " " ) break;
		$title = substr ( $title, 0, $ind );
	}				
	return $title;	
}

//	------------ Display immagini artisti nelle colonne di destra -------------
function showImages ( $imgarr )
{
	$imgind = 0;
	foreach ( $imgarr as $image ) {  
		if ( (++ $imgind % 2) == 1 )  echo "<tr>";
		if ( $image[0] == '#' ) {
			$search = str_replace ("#", " ", $image);
			if ( strlen($search) > 70 )
				$search = substr ($search, 0, 70);
			echo "<td><div class='img-ground'>$search</div></td>";
	   	}
	   	else
   			echo "<td><img class='img-edge' src=$image></td>";
   		if ( ($imgind % 2) == 0 )  echo "</tr>";
   	}
   	if ( ($imgind % 2) == 1 )  echo "</tr>";			
}

//	------------ Cerca l'immagine dell'artista ---------------------------------
function imageFinder ( $strweb, $singer )
{
	$image = "";
	$imgind = strpos( $strweb, "http://userserve", 0 );

	if ( $imgind !== false ) 
	{
		$strtemp = substr( $strweb, $imgind, 100 );
		$imgend = strpos( $strtemp, ".jpg", 0 );	
		if ( $imgend === false )
			$imgend = strpos( $strtemp, ".png", 0 );	
		if ( $imgend === false )
			$imgend = strpos( $strtemp, ".gif", 0 );	
			
		if ( $imgend !== false )			
			$image = substr ( $strtemp, 0, $imgend+4 );
	}
	if ( $image == "" )
		$image = "#". $singer;
		
	return $image;
}

//	------------ Restiruisce il numero totale di tracce -----------------------
function countSelect ( )
{
	$count = 0;
	$stind = 1;
	
	if ( plistActive() )
	{
		$plist = new Plist ( );
		$stend = $plist->ply_total ( );
	 
		while ( $stind <= $stend ) {
			if ( $plist->ply_ischecked ( $stind ) )
				$count ++;
			$stind ++;
		}	
		unset ( $plist );		
	}
	else
	{
		$track = new Track ( );
		$stend = $track->trk_total ( );
	 
		while ( $stind <= $stend ) {
			if ( $track->trk_ischecked ( $stind ) )
				$count ++;
			$stind ++;
		}	
		unset ( $track );
	}
	return $count;
}

//	------------ Visualizza messaggio di warning ------------------------------
function msgWarning ( $str, $option=0 )
{
	$color = ($option == END_PAGE) ? "btn-gray" : "btn-warn";
?>
	<div id="my-warning">
		<button type="button" class="btn <?= $color ?> btn-lg">
       		<strong>&nbsp <?= $str ?> &nbsp</strong>
    	</button> 
    </div>
<?php
}

//	------------ Ritrasforma caratteri speciali inseriti in Database ----------
function metaCharSet ( $str )
{
	$str = str_replace ("\x18", "+", $str);
	$str = str_replace ("\x19", "=", $str);
	$str = str_replace ("\x1C", "&", $str);
	$str = str_replace ("\x1D", "#", $str);
	
	return $str;
}

//	------------ Trim dei commenti nelle query string -------------------------
function trimQueue ( $xmlstring ) 
{
	if ( ($ind = strpos ($xmlstring, "<", 0)) !== false )
		return substr ($xmlstring, 0, $ind);
	else
		return $xmlstring;
}

//	------------ Login / Logout / User control --------------------------------
function userLogin ( $option=GET_USER )
{
	if ( $option == GET_USER )
		return $_SESSION["user_login"];
	else
		$_SESSION["user_login"] = $option;
}

//	------------ Restituisce il numero di tracce per pagina -------------------
function loadLimit ( )
{
	return $_SESSION['load_limit'];
}

//  ------------ Restituisce il tipo di tracce da scartare --------------------
function loadSkip ( )
{
	return $_SESSION['load_skip'];
}  

//	------------ Restituisce il numero totale di tracce -----------------------
function trackTotal ( )
{
	return $_SESSION['track_count'];
}

//	------------ Restituisce presenza di playlist attiva ---------------------
function plistActive ( )
{
	return $_SESSION["init_plist"];
}
?>
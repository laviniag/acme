<?php 
function webSongs ( $singer, $wpage )
{
	$track = new Track ( );
	if ( $wpage == DEF_ZERO ) 									//  controlli di inizio ricerca
	{
		if ( $singer == "" )
			return "";
		elseif ( strlen ($singer) < 2 )
			return "PAROLA DI RICERCA TROPPO CORTA";
		elseif ( $singer == $track->trk_singer() ) {
			echo "<script> window.location='music.php' </script>";
			return "";
	    }
    }
		
	$artist = remove_accents ( $singer );
    $urlsearch = "http://ololo.fm/search/$artist";
    if ( $wpage != DEF_ZERO ) $urlsearch .= "/$wpage";
// echo "URLsearch= $urlsearch | Wpage= $wpage<br>";
	
	if ( ($strweb = @file_get_contents($urlsearch)) === false )
		return "LA RICERCA NON HA DATO RISULTATI";

	$start = strpos( $strweb, "data-url=", 0);
	if ( $start === false ) {
		if ( $wpage == DEF_ZERO )
			return "LA RICERCA NON HA DATO RISULTATI";
		else {
			$track->trk_newpage ( DEF_ZERO );	
			return "";
		}
	}
	$track->trk_newpage ( $wpage );	

	if ( $wpage == DEF_ZERO ) 									//  cerca l'immagine dell'artista
	{		
		$track->trk_singer ( $singer );											
		$track->trk_setblock ( SET_START );
		$image = imageFinder ( $strweb, $singer );
		if ( $image != "" )
			$track->trk_setimage ( $singer, $image );
	}

	while ( $start = strpos( $strweb, "data-url=", $start) ) 
	{	
		$start += 10;
		$stend = strpos ( $strweb, "\"", $start );				//  cerca riferimento alla canzone
		$track->song = substr ( $strweb, $start, $stend-$start );
	
		$start = $stend;
		$start = strpos ( $strweb, "resource=", $start );		//  cerca codice risorsa canzone
		$start += 10;
		$stend = strpos ( $strweb, "\"", $start );
		$track->save = substr ( $strweb, $start, $stend-$start );
		
		$tind = strpos ( $strweb, "&laquo;", $start );	//  cerca il titolo
		$tind += 7;
		$tend = strpos ( $strweb, "&raquo;", $tind );		
		$title = substr ( $strweb, $tind, $tend-$tind );

		if ( $track->title = titleFilter ($title) )
		{
			if ( $tind = strpos ($strweb, "<dfn>", $start-800) )
			{
				$tind += 5;
				$tend = strpos ($strweb, "</dfn>", $tind);
				$timer = " [". substr ($strweb, $tind, $tend-$tind) ."]";
				$track->title .= " &nbsp " . $timer;
			}
			if ( substr ($timer,2,2) > loadSkip() )	
				$track->trk_puts ( $track->title, $track->save, $track->song );
		}
	}
	$track->trk_setblock ( END_PAGE );
	unset ($track);
	
	if ( $wpage == DEF_ZERO ) {
		echo "<script> window.location='music.php?show' </script>";
	}
	return "";
}
?>
<?php
session_start();
//   --------------- Modulo che memorizza i settaggi delle checkbox
include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_bundle/track_class.php";
include_once "../mf_store/plist_class.php";

if ( isset ($_REQUEST["func"]) )
{
    $inputstring = ($_REQUEST["func"]);
    $querystring = trimQueue ($inputstring);
	
	$chkarr = json_decode ($querystring);

	foreach( $chkarr as $ind )
	{
		if ( $ind == 0 ) continue;
		
		if ( plistActive() )
			$_SESSION['plist_check'][abs($ind)] = ($ind > 0) ? true : false;
		else
			$_SESSION['track_check'][abs($ind)] = ($ind > 0) ? true : false;
	}
	
	echo countSelect();
}
elseif ( isset ($_REQUEST["play"]) )
{	
	$list = array ();
	$checkfound = false;
	
	if ( plistActive() )
	{
		$plist = new Plist ( );
		$trkind = 1;
		$trkend = $plist->ply_total ( );
		$artwork = "totalcontrol/images/artwork.png";

		while ( $trkind <= $trkend )
		{
			if ( $plist->ply_ischecked ( $trkind ) )
			{
				$checkfound = true;
				$plist->ply_actual ( $trkind );
				$display = titleArtist ( $plist->title );
					
				$list[] = array('mp3' => $plist->song, 'artist' => $display[0],
								'title' => $display[1], 'artwork' => $artwork );
			}
			$trkind ++;
		}	
		unset ( $plist );
	}
	else
	{
		$track = new Track ( );
		$trkind = 1;
		$trkend = $track->trk_total ( );
		$artwork = "totalcontrol/images/artwork.png";

		while ( $trkind <= $trkend )
		{
			if ( $track->trk_ischecked ( $trkind ) )
			{
				$checkfound = true;
				$track->trk_actual ( $trkind );
				$display = titleArtist ( $track->title );
					
				$list[] = array('mp3' => $track->song, 'artist' => $display[0],
								'title' => $display[1], 'artwork' => $artwork );
			}
			$trkind ++;
		}	
		unset ( $track );
	}	
		
	if ( ! $checkfound )
		$list[] = array('mp3' => '', 'artist' => '', 'title' => '', 'artwork' => '');
		
	echo json_encode($list);
}
?>
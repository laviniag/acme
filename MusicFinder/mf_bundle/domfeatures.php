<?php 
//	---------- Modulo che predispone il DOM per gli artisti in evidenza
function domFeatures ( $resp )
{
	if ( $resp != "" ) {
		msgWarning ( $resp );
	}
?>
	<h3 id="feat-head" class="butt-hide">Artisti in evidenza questa settimana</h3>
	<br><br>
	<form name="input_feat" action="">
	<div class="row">
<?php
	for ( $ind=0; $ind < FEAT_LIMIT; $ind++ ) 
	{	
		$idbt = "bt". $ind;
		$idimg = "img". $ind;
		$idsp = "sp". $ind;
?>
		<div class="col-sm-3 col-md-3 style="display: inline-block">
		<button type="submit" id="<?= $idbt ?>" class="butt-hide" name="singer" value="">
			<img id="<?= $idimg ?>" class="img-feat" src=""><br>
			<span id="<?= $idsp ?>" class="font-artist"></span>
		</button>
		</div>
<?php
		if ( (($ind+1) % 4) == 0 )
			echo "</div><br><div class='row'>";
	}
?>
	</div>
	</form>
<?php	
}
?>
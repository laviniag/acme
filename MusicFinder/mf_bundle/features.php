<?php 
//   ------------- Modulo che cerca sul web gli artisti in evidenza
include_once "../mf_bundle/constants.php";

$list = array ();
$urlsearch = "http://www.notiziemusicali.it/artistimusica.php";
$xmldata = "<?xml version='1.0' encoding='UTF-8'?><mf_list>";

if ( $strweb = @file_get_contents($urlsearch) ) 
{
	if ( strpos ($strweb, "/artista/", 0) )
	{
		$count = $start = 0;
		while ( $start = strpos($strweb, "/artista/", $start) ) 
		{	
			$start += 9;
			$stend = strpos ( $strweb, "/", $start);		//  cerca riferimento alla canzone
			$artist = substr ($strweb, $start, $stend-$start);
			$artist = str_replace("-", " ", $artist);
			
			$tind = strpos( $strweb, "img src=", $start);	//  cerca il titolo
			$tind += 9;
			$tind = strpos( $strweb, "=", $tind) + 1;		
			$tend = strpos( $strweb, "&", $tind);		
			$image = substr ($strweb, $tind, $tend-$tind);
			
			$xmldata .= "<feat><artist>$artist</artist>";
			$xmldata .= "<image>$image</image></feat>";

			$start = $tend;
			if ( ++$count == FEAT_LIMIT )  break;
		}
	}
}

$xmldata .= "</mf_list>";
echo $xmldata;
?>
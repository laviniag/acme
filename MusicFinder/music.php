<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Music Finder</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"  
		  http-equiv="Content-type" charset="ISO-8859-15" />
	<link rel="shortcut icon" href="images/music-note.ico" />
    <!-- Total Control HTML5 Audio Player -->
  	<link href="./jquery/style/jquery-ui.css" rel="stylesheet">
  	<link href="./totalcontrol/style/jquery.jscrollpane.css" rel="stylesheet">
    <!-- Bootstrap stylesheet -->
	<link href="./bootstrap/css/button_3D.css" rel="stylesheet" media="screen">   
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./bootstrap/css/login_form.css" rel="stylesheet" media="screen">
    <link href="./bootstrap/css/stylesheet.css" rel="stylesheet" media="screen">    
    <!-- jQuery (for Total Control HTML5 Audio Player) -->
    <script src="./jquery/jquery-1.6.4.min.js"></script>  
    <script src="./jquery/jquery-ui-1.11.2.min.js"></script>  
<script src="./totalcontrol/js/soundmanager/script/soundmanager2.js" type="text/javascript"></script>
<script src="./totalcontrol/js/jscrollpane/script/jquery.mousewheel.js" type="text/javascript"></script>
<script src="./totalcontrol/js/jscrollpane/script/jquery.jscrollpane.js" type="text/javascript"></script>
    <script src="./totalcontrol/totalcontrol.js"></script>
    <!-- Include all compiled bootstrap javascripts  -->
    <script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./mf_script/audioplay.js" type="text/javascript"></script> 
<?php
session_start();
include_once "fancybox/fancybox.html";
?>
</head>
<body>
<div class="container">
<script type='text/javascript'> initAudio(1); </script>
<form role="form" action="music.php" method="POST" name="fill_page">

<?php
include_once "mf_bundle/constants.php";
include_once "mf_bundle/track_class.php";
include_once "mf_bundle/functions.php";
include_once "mf_bundle/buttons.php";
include_once "mf_bundle/websongs.php";
include_once "mf_bundle/noaccent.php";
init_session ( );

// ------------ Di default esegue il refresh della pagina ---------------------
$option = SET_RELOAD;

if ( isset ($_REQUEST['show']) )
	$option = SET_START;
elseif ( isset ($_REQUEST['back']) )
	$option = BACKWARD;
elseif ( isset ($_REQUEST['forw']) )
	$option = FORWARD;
	
// ----------------------------------------------------------------------------
	$track = new Track ( );
if ( $track->trk_total() > 0 )
{
	$track->trk_getblock ( $trkind, $trkend );
// echo "||| $trkind | $trkend <<<";
	
	if ( $option == SET_RELOAD || $option == FORWARD )
	{
		$trkind = $track->trk_toppage ( );
		if ( $option == FORWARD )	
			$trkind += loadLimit ( );	
			
		$trkend = $trkind + loadLimit() - 1;
		if ( $trkend > $track->trk_total() ) {
			webSongs ( $track->trk_singer(), $track->trk_newpage() );
		}			
		if ( $trkend > $track->trk_total() )
			$trkend = $track->trk_total();			
	}
	elseif ( $option == BACKWARD )
	{
		$trkind = $track->trk_toppage() - loadLimit();
		if ( $trkind < 1 ) 
			$trkind = 1;
			
		$trkend = $trkind + loadLimit() - 1;		
		if ( $trkend > $track->trk_total() )
			$trkend = $track->trk_total();
	}
	elseif ( $option == SET_START )
	{
		$trkjnd = $trkind + loadLimit() - 1;
		if ( $trkjnd < $trkend ) { 
			$trkend = $trkjnd;
		} 
	}
	
// echo ">>> $trkind | $trkend <br>";	
	$track->trk_toppage ( $trkind );	
	$track->trk_endpage ( $trkend );	
		
	disp_buttons ( $trkind, $trkend, $track->trk_total() );

	if ( $trkind > $trkend ) { 
		msgWarning ( "&nbsp NON CI SONO ALTRI RISULTATI &nbsp", END_PAGE );
	}	
?>	
	<div class="row">
	<div class="col-sm-4 col-md-4 col-md-push-8 to-stepdown">
		<div id="my-dialog" class="my-alert-bold"></div>
		<div id="my-loading" class="my-dialog-bold"></div>
		
		<div id="play-box" class="butt-hide">
			<ul id="play-sound" style="margin-left:auto; margin-right:auto;">
			</ul>
		</div>
		<div id="image-list" class="butt-show">
			<table class="tab-side">
<?php	
			$imgarr = $track->trk_getimages ( );
			showImages ( $imgarr );
?>
			</table>
		</div>
	</div>
		
	<div class="col-sm-8 col-md-8 col-md-pull-4" style="display: inline-block">	
	<ul>
<?php
	for ( $ind = $trkind; $ind <= $trkend && $track->trk_actual($ind) != END_PAGE; $ind++ ) 
	{	
		$idaudio = "au" . $ind;	
		$idbox = "ck" . $ind;
		$imgname = "im" . $ind;
		$flag = ( $track->trk_ischecked($ind) ) ? "checked" : "";					
		
		$display = titleArtist ( $track->title );
?>  	
        <li class="high-line">
        <div class="checkbox">
   			<input type="checkbox" id="<?= $idbox ?>" <?= $flag ?> name="chkarr[<?= $ind ?>]" value=1>
           	<p class="to-shadow" onclick="showTrack(<?= $ind ?>)">
           		<?= $track->title ?>
           		<img id="<?= $imgname ?>" class="to-endline" src="images/button_play.png">
			</p>
        	<div class="to-right" style="display: none">
    	  		<ul id="<?= $idaudio ?>" style="list-style-type: none;">
 					<li mp3="<?= $track->song ?>" artist="<?= $display[0] ?>" 
					title="<?= $display[1] ?>" artwork=""></li>
				</ul>	
			</div>			
		</div>
		</li>				
<?php
	}
	unset ($track);	
?>
	</ul>
	</div>
	</div>
	<div id="box-start" class="butt-hide"><?= $trkind ?></div>
	<div id="box-stend" class="butt-hide"><?= $trkend ?></div>
<?php
}
?>
</form>
</div>
</body>
</html>
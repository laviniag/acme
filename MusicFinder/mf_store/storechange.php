<?php
session_start();
//  Modulo che unisce playlist / elimina playlist nel Database
//	-------- Se merge: unisce 2 o più playlist con nu nuovo nome --------------
//	-------- Se delete: cancella 1 o più playlist esistenti -------------------

include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_store/dbms.php";

$store = new Dbms( );
$store->OpenDatabase ( );						//  Connessione al Database 
$iduser = userLogin ( );
$listname = "Nuova playlist";

if ( isset ($_REQUEST["merge"]) ) 
{
	$inputstring = ($_REQUEST["merge"]);
    $plyarr = json_decode (trimQueue ($inputstring));
 
    if ( isset ($_REQUEST["name"]) ) {
    	$inputstring = ($_REQUEST["name"]);
		$listname = trimQueue ($inputstring);
	}
   	
	$newplay = $store->InsertInto ("mf_playlist (iduser,playname)", "($iduser,'$listname')");
 
	foreach ( $plyarr as $idplay )
	{
		$query = "SELECT idsong FROM mf_bridge 
				  INNER JOIN mf_playlist ON mf_playlist.idplay=mf_bridge.idplay 
			  	  WHERE mf_playlist.iduser=$iduser AND mf_playlist.idplay=$idplay";
		$result = $store->RunQuery ( $query );
		while ( ($bridge = $store->NextItem ($result)) )
		{
			$store->InsertInto ("mf_bridge (idplay, idsong)", "($newplay, $bridge[0])");
		}
	}	
} 
elseif ( isset ($_REQUEST["delete"]) )
{
	$inputstring = ($_REQUEST["delete"]);
	$plyarr = json_decode (trimQueue ($inputstring));
	
	foreach ( $plyarr as $idplay )
	{
		$store->DeleteRecord ( "mf_bridge", "idplay=$idplay" );
		$store->DeleteRecord ( "mf_playlist", "idplay=$idplay" );
	}	
} 
elseif ( isset ($_REQUEST["rename"]) )
{
	$idplay = ($_REQUEST["rename"]);
    if ( isset ($_REQUEST["name"]) ) {
    	$inputstring = ($_REQUEST["name"]);
		$listname = trimQueue ($inputstring); 
	}

	$store->UpdateTable ( "mf_playlist", "playname='$listname'", "idplay=$idplay" );
} 

$store->CloseDatabase ();
unset ( $store );
?>
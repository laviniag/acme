<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Merge Playlist</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta http-equiv="Content-type" charset="ISO-8859-15" />
    <!-- Bootstrap stylesheet -->
	<link href="../bootstrap/css/button_3D.css" rel="stylesheet" media="screen">   
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../bootstrap/css/login_form.css" rel="stylesheet" media="screen">
    <link href="../bootstrap/css/stylesheet.css" rel="stylesheet" media="screen">    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../jquery/jquery-1.11.1.min.js"></script>
    <!-- Include all compiled bootstrap plugins  -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../mf_script/saveplist.js" type="text/javascript"></script>
</head>
<body>
<div class="container">

<?php
session_start();
include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_store/plist_class.php";
include_once "../mf_store/dbms.php";

	$store = new Dbms( );
	$store->OpenDatabase ( );						//  Connessione al Database 
	$iduser = userLogin ( );
	
	$plist = new Plist ( );
	$idactive = $plist->ply_current ( );
?>
<form class="form" role="form" name="merge_play" method="post" action="">
<div class="well text-center">

<?php
	$query = "SELECT idplay, playname FROM mf_playlist WHERE iduser=$iduser";
	$result = $store->RunQuery ( $query );
	if ( ($play = $store->NextItem ($result)) ) {
?>
		<div class="row">
			<button type="button" class="btn btn-fresh">
				<strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SELEZIONA&nbsp LE&nbsp PLAYLIST 
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</strong>
			</button>    
		</div>
    	<div class="row">
<?php
		$ind = 0;
		do {
			$flag = ($play[0] == $idactive) ? "checked" : "";
			$playname = metaCharSet ( $play[1] );
			$idrename = "id". $ind;
?>
			<div class="checkbox">
			  	<div class="to-left">
   					<input type="checkbox" <?= $flag ?> name="plyarr" value="<?= $play[0] ?>">
   					<span id="<?= $idrename ?>"> <?= $playname ?></span></input>
        	  	</div>
        	</div>
<?php
			$ind ++;
		} while ( ($play = $store->NextItem ($result)) );
?>
		</div>
<?php
	}	
?>
	<div class="my-alert" id="alert-box"></div>
<?php
	$store->CloseDatabase ();
	unset ( $store );
	unset ( $plist );
	
	if ( $ind > 1 ) {
?>
	   	<div class="row">
			<button type="button" class="btn btn-sky" onclick="plistControl(1)">
				<strong>&nbsp UNISCI LE PLAYLIST SELEZIONATE &nbsp</strong>
			</button> 
	    </div>
<?php
	}
?>  
    <br>
   	<div class="row">
		<div class="form-group to-middle">
            <label class="sr-only" for="InputPlay"></label>
            <input type="text" id="InputPlay" class="form-control" 
        	name="playname" placeholder="nuovo nome playlist" required>
        </div>
    </div> 

   	<div class="row">
		<button type="button" class="btn btn-sunny" onclick="plistControl(2)">
			<strong>&nbsp RINOMINA PLAYLIST SELEZIONATA &nbsp</strong>
		</button> 
    </div>
    <br>
   	<div class="row">
		<button type="button" class="btn btn-hot" onclick="plistControl(3)">
			<strong>&nbsp ELIMINA LE PLAYLIST SELEZIONATE &nbsp</strong>
		</button> 
    </div>       
    <br>
    <div class="my-dialog-bold" id="dialog-box"></div>
   	<div class="row">
   	<div id="list-ok" class="butt-hide">
		<button type="button" id="id-exit" onclick="parent.jQuery.fancybox.close();">
			<strong>&nbsp OPERAZIONE COMPLETATA &nbsp</strong>
			<i class="glyphicon glyphicon-remove-circle"></i>
		</button> 
    </div>
    </div>
</div> 
</form>  
</div>
</body>
</html>	
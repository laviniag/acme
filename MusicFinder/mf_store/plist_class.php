<?php
/*
    Classe PLIST: gestisce le ricerche tramite arrays della SESSION
*/
class Plist
{
    //----- Inizializzazioni mediante costruttore -----------------------
    function __construct()
    {
        $this->check = false;
        $this->idart = 0;
        $this->title = "";
        $this->song = "";
    }
    
//------- Inserisce una nuova traccia in Sessione -----------------------
    function ply_puts ( $idart, $title, $song )
    {
   		$index = $_SESSION['plist_count'] + 1;
   		$_SESSION['plist_count'] = $index;
 	    $_SESSION['plist_check'][$index] = true;  
 	    $_SESSION['plist_idart'][$index] = $idart;  
 	    $_SESSION['plist_title'][$index] = $title;
 	    $_SESSION['plist_song'][$index] = $song;   
    }
      
//------- Fetch della traccia corrente ----------------------------------
    function ply_actual ( $trkind )
    {  		
    	if ( $_SESSION['plist_count'] >= $trkind ) 
    	{    
        	$this->check = $_SESSION['plist_check'][$trkind];    	
	        $this->idart = $_SESSION['plist_idart'][$trkind];
	        $this->title = $_SESSION['plist_title'][$trkind];
	        $this->song = $_SESSION['plist_song'][$trkind];
        	return OK_CODE;
	  	} else
	  		return END_PAGE;        
    }
    
//------- Setta il nome dell'artista cercato ----------------------------
    function ply_current ( $idplay=0 )
    {
    	if ( $idplay == 0 )
    		return $_SESSION['plist_current'];
    	else
 	    	$_SESSION['plist_current'] = $idplay;  
    } 
     
//------- Riassegna il campo url della canzone --------------------------
    function ply_assign ( $trkind, $song )
    {
 	    $_SESSION['plist_song'][$trkind] = "http://ololo.fm/download/". $song; 
 	    $_SESSION['plist_idart'][$trkind] = DEF_ZERO; 
    }
      
//------- Toggle dello stato della traccia on/off -----------------------
    function ply_check ( $trkind, $onoff )
    {
   		$_SESSION['plist_check'][$trkind] = $onoff; 
    }
                      
//------- Restituisce lo stato del checkbox on/off ---------------------
    function ply_ischecked ( $trkind )
    {
    	return $_SESSION['plist_check'][$trkind];
    } 
    
//------- Restituisce il numero complessivo di tracce -------------------
    function ply_total ( )
    {
    	$total = $_SESSION['plist_count'];
   		return $total;
    }
    
//------- Mette il puntatore di inizio/fine pagina ----------------------
    function ply_setblock ( $flag )
    {
    	if ( $flag == SET_START ) {
    		$_SESSION['plist_top'] = $_SESSION['plist_count'] + 1;
	    } else
	    	$_SESSION['plist_end'] = $_SESSION['plist_count'];
    }   
    
//------- Restituisce puntatore di inizio/fine pagina -------------------
    function ply_getblock ( &$block1, &$block2 )
    {
    	if ( $_SESSION['plist_top'] > 0 ) 
    	{
	  		$block1 = $_SESSION['plist_top'];
	  		$block2 = $_SESSION['plist_end']; 
	  		return true;
	  	} else
	  		return false;
    }
    
//------- Setta la nuova pagina da cercare sul web ----------------------
    function ply_newpage ( $wpage=GET_PAGE )
    {
    	if ( $wpage == GET_PAGE )
    	{
    		$npage = $_SESSION['plist_lastpage'] + 1;
    		$_SESSION['plist_lastpage'] = $npage;
  			return $npage;
  		} else
  			$_SESSION['plist_lastpage'] = $wpage;
    }    
        
//------- Setta punto d'inizio della pagina visualizzata ----------------
    function ply_toppage ( $wcount=GET_PAGE )
    {
    	if ( $wcount == GET_PAGE )
    		return $_SESSION['pagelist_top'];
    	else {
  			$_SESSION['pagelist_top'] = $wcount;
    	} 
    }   
        
//------- Setta punto finale della pagina visualizzata ------------------
    function ply_endpage ( $wcount=GET_PAGE )
    {
    	if ( $wcount == GET_PAGE )
    		return $_SESSION['pagelist_end'];
    	else {
  			$_SESSION['pagelist_end'] = $wcount;
    	} 
    } 
    
//------- Memorizza l'immagine dell'artista -----------------------------
    function ply_setimage ( $imgind, $refer )
    {
    	$_SESSION['plist_artist'][$imgind] = $refer[0];  
    	$_SESSION['plist_image'][$imgind] = $refer[1];  
// echo (">> ".$imgind." | ".$refer[0]." | ".$refer[1]."<br>");
    }

//------- Restituisce array con nomi artisti da cercare -----------------
    function ply_getartists ( )
    {
    	$outarr = $_SESSION['plist_artist'];
   		return $outarr;
    }    

//------- Restituisce array con immagini degli artisti ------------------
    function ply_getimages ( )
    {
    	$outarr = $_SESSION['plist_image'];
   		return $outarr;
    }    
}
?>
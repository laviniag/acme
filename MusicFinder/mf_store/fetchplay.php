<?php
//------- Preleva dal Database la playlist di cui abbiamo il nome -----------------------
function fetchPlay ( $listname )
{
	$store = new Dbms( );
	$store->OpenDatabase ( );						//  Connessione al Database 
	$iduser = userLogin ( );	
	
	$imgind = 0;
	$plist = new Plist ( );
	$plist->ply_setblock ( SET_START );
	
	$query = "SELECT idplay FROM mf_playlist WHERE iduser=$iduser AND playname='$listname'";
	$result = $store->RunQuery ( $query );
	if ( ($response = $store->NextItem ($result)) ) 
	{
		$idplay = $response[0];
		$imgind = singlePlay ( $store, $plist, $idplay, $imgind );
	}	

	$plist->ply_setblock ( END_PAGE );
	unset ( $plist );
	
	$store->CloseDatabase ();
	unset ( $store );
}

//------- Restituisce il nome della playlist corrente -----------------------------------
function namePlay ( )
{
	$store = new Dbms( );
	$store->OpenDatabase ( );						//  Connessione al Database 
	$iduser = userLogin ( );
	
	$plist = new Plist ( );	
	$idplay = $plist->ply_current ( );
	$playname = "";

	$query = "SELECT playname FROM mf_playlist WHERE iduser=$iduser AND idplay=$idplay";	
	$result = $store->RunQuery ( $query );
	if ( ($response = $store->NextItem ($result)) ) 
	{
		$playname = metaCharSet ($response[0]);
	}
	
	unset ( $plist );
	$store->CloseDatabase ();
	unset ( $store );
	
	return $playname;
}

//------- Preleva la singola playlist dal Database --------------------------------------
function singlePlay ( $store, $plist, $idplay, $imgind )
{
	$idphoto = 0;
	$iduser = userLogin ( );
	$plist->ply_current ( $idplay );
	
	$query = "SELECT idphoto, songtitle, songlink FROM mf_songs  
			  INNER JOIN mf_bridge ON mf_bridge.idsong=mf_songs.idsong 
			  INNER JOIN mf_playlist ON mf_playlist.idplay=mf_bridge.idplay 
			  WHERE mf_playlist.iduser=$iduser AND mf_playlist.idplay=$idplay";
	$result = $store->RunQuery ( $query );
	while ( ($refer = $store->NextItem ($result)) )
	{
		$plist->ply_puts ( $refer[0], $refer[1], $refer[2] );
		
		if ( $idphoto != $refer[0] ) {
			$idphoto = $refer[0];
			$query = "SELECT artist, photolink FROM mf_photos WHERE idphoto=$idphoto";
			$respho = $store->RunQuery ( $query );
			if ( ($refer = $store->NextItem ($respho)) )
				$plist->ply_setimage ( $idphoto, $refer );
		}
	}
}
?>
<?php
session_start();
//   Modulo che controlla username e password nel Database
include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_store/dbms.php";

if ( isset ($_REQUEST["logout"]) )
{
	userLogin ( LOG_OUT );
	echo "OK";
}
else
{
	$user = ( isset ($_REQUEST["user"]) ) ? $_REQUEST["user"] : "";
	$pass = ( isset ($_REQUEST["pass"]) ) ? $_REQUEST["pass"] : "";
	$reg = ( isset ($_REQUEST["reg"]) ) ? $_REQUEST["reg"] : NEW_USER;
	$controls = true;
	
	if ( $user == "" ) {
		echo "Inserisci e-mail";
		$controls = false;
 	}  
 	elseif ( ! ($controls = filter_var($user, FILTER_VALIDATE_EMAIL)) ) {
		echo "Formato e-mail non valido";
 	}  

 	if ( $controls && $pass == "" ) {
		echo "Inserisci Password";
		$controls = false;
	}
 	
	if ( $controls )
	{
		$store = new Dbms( );
		$store->OpenDatabase ( );						//  Connessione al Database 

		$query = "SELECT iduser FROM mf_users WHERE nickname='$user'";
		$result = $store->RunQuery ( $query );
		if ( $store->HayResults ($result) )
		{
			if ( $reg == NEW_USER ) {
				echo "Utente già registrato!";
			}
			else {
				$query = "SELECT iduser FROM mf_users WHERE nickname='$user' AND password='$pass'";
				$result = $store->RunQuery ( $query );
				if ( ($resp = $store->NextItem ($result)) ) {
					userLogin ( $resp[0] );
					echo $resp[0];
				} 
				else {
					echo "Password errata!";
				}
			}
		}
		else {
			if ( $reg == NEW_USER )
			{
				$iduser = $store->InsertInto ("mf_users (nickname,password)", "('$user','$pass')");
				userLogin ( $iduser );
				echo $iduser;
			}
			else {
				echo "Non sei ancora registrato!";
			}
		}   
		    
		$store->CloseDatabase ();
		unset ( $store );
	}
}   
?>
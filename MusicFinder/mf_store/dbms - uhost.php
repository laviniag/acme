<?PHP
/*
    DATA ACCESS LAYER - DataBase Management
*/
class Dbms
{
//      Apre il database di nome DBname ed esegue i controlli di base
    function OpenDatabase ( )
    {
        $dbhostname = "mysql.uhostfull.com";
        $dbname = "u819137388_mf";
        $dbusername = "u819137388_gaya";
        $dbpassword = "Nyag-40";

        $dbconnect = mysql_connect ($dbhostname, $dbusername, $dbpassword);
        if ( ! $dbconnect ) {
            die('Connection not established : ' . mysql_error());
        }
//      Salvo il pointer di connessione nella Session
        $_SESSION["dbconnect"] = $dbconnect;

        $dbselected = mysql_select_db ($dbname, $dbconnect);
        if ( ! $dbselected ) {
            die ('Database not connected : ' . mysql_error());
        }
    }

//      Effettua una query di ricerca (impostata in DBbuffer)
    function RunQuery ( $dbbuffer )
    {
        return mysql_query ($dbbuffer);
    }

//      Restituisce la riga successiva della query di ricerca precedente
    function NextItem ( $dbview )
    {
        if ( $dbview != Null )
            return mysql_fetch_row ($dbview);
         else
            return Null;
    }

//      Restituisce il numero di record che soddisfano la ricerca precedente
    function HowManyItems ( $dbview )
    {
        if ( $dbview != Null )
            return mysql_num_rows ($dbview);
        else
            return 0;
    }

//      Verifica se il risultato di una query è nullo
    function HayResults ( $dbview )
    {
        if ( mysql_num_rows ($dbview) == 0 )
            return false;
        else
            return true;
    }

//      Inserisce un nuovo record in una data tabella
    function InsertInto ( $dbfields, $dbvalues )
    {
        $querystr = "INSERT INTO $dbfields  VALUES $dbvalues";

        ( $result = mysql_query ($querystr) )
            or die("Query di Insert non valida: " . mysql_error());

        return mysql_insert_id();
    }

//      Update di un record esistente in una data tabella
    function UpdateTable ( $dbtable, $dbvalues, $dbcond )
    {
        $querystr = "UPDATE $dbtable SET $dbvalues WHERE $dbcond";

        ( $result = mysql_query ($querystr) )
            or die("Query di Update non valida: " . mysql_error());
    }

//      Cancellazione di un record esistente in una data tabella
    function DeleteRecord ( $dbtable, $dbcond )
    {
        $querystr = "DELETE FROM $dbtable WHERE $dbcond";

        ( $result = mysql_query ($querystr) )
            or die("Query di Delete non valida: " . mysql_error());
    }

//      Chiusura del DataBase precedentemente aperto
    function CloseDatabase ()
    {
        $dbconnect = $_SESSION["dbconnect"];
        if ( ! mysql_close($dbconnect) ) {
            die('DB Close impossible : ' . mysql_error());
        }
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Save Playlist</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
	<meta http-equiv="Content-type" charset="ISO-8859-15" />
    <!-- Bootstrap stylesheet -->
	<link href="../bootstrap/css/button_3D.css" rel="stylesheet" media="screen">   
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../bootstrap/css/login_form.css" rel="stylesheet" media="screen">
    <link href="../bootstrap/css/stylesheet.css" rel="stylesheet" media="screen">    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../jquery/jquery-1.11.1.min.js"></script>
    <!-- Include all compiled bootstrap plugins  -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../mf_script/saveplist.js" type="text/javascript"></script>
</head>
<body>
<div class="container">

<?php
session_start();
include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_store/dbms.php";

	$store = new Dbms( );
	$store->OpenDatabase ( );						//  Connessione al Database 
	$iduser = userLogin ( );
?>
    <form class="form" role="form" name="save_play" method="post" action="">
   	<div class="well text-center">
   	
   	<div class="row">
		<button type="button" class="btn btn-hot" onclick="savePlist(1)">
			<strong>&nbsp NUOVA PLAYLIST &nbsp</strong>
		</button> 
    </div>
    <br>
    <div class="my-alert" id="alert-1"></div>
    
   	<div class="row">
		<div class="form-group to-middle">
            <label class="sr-only" for="InputPlay"></label>
            <input type="text" id="InputPlay" class="form-control" 
        	name="playname" placeholder="nome playlist" required>
        </div>
    </div> 
    <br>
	<div class="row">
<?php
	$query = "SELECT idplay, playname FROM mf_playlist WHERE iduser=$iduser";
	$result = $store->RunQuery ( $query );
	if ( ($play = $store->NextItem ($result)) ) {
?>
			<button type="button" class="btn btn-sunny" onclick="savePlist(2)">
				<strong>&nbsp AGGIUNGI A PLAYLIST &nbsp</strong>
			</button>    
		</div>
		<div class="my-alert" id="alert-2"></div>
		
    	<div class="row">
    		<!-- Radio-Button vuoto per creare l'array id_play -->
    		<input type="radio" name="id_play" class="butt-hide"></input>
<?php
		do {
			$playname = metaCharSet ( $play[1] );
?>
			<div class="radio">
			  <div class="to-left">
				<label>
				<input type="radio" name="id_play" value="<?= $play[0] ?>"><?= $playname ?>
        		</label>
        	  </div>
        	</div>
<?php
		} while ( ($play = $store->NextItem ($result)) );
	}
	$store->CloseDatabase ();
	unset ( $store );	
?>  
	<br>  
	</div>     
      
   	<div class="row">
   	<div id="list-ok" class="butt-hide">
		<button type="button" class="btn btn-sky" onclick="parent.jQuery.fancybox.close();">
			<strong>&nbsp PLAYLIST SALVATA &nbsp</strong>
			<i class="glyphicon glyphicon-remove-circle"></i>
		</button> 
    </div>
    </div>
    
	</div> 
    </form>  
</div>
</body>
</html>	
<?php
session_start();
//  Modulo che visualizza playlist / salva le playlist nel Database
//	-------- Se playname = nome: salva playlist nel Database ------------------
//	-------- Se idplay = ID: append nella playlist esistente ------------------

include_once "../mf_bundle/constants.php";
include_once "../mf_bundle/track_class.php";
include_once "../mf_bundle/functions.php";
include_once "../mf_store/dbms.php";

$idplay = 0;
$listname = "Nuova playlist";

if ( isset ($_REQUEST["play"]) )
	$idplay = $_REQUEST["play"];
if ( isset ($_REQUEST["name"]) ) {
    $inputstring = ($_REQUEST["name"]);
	$listname = trimQueue ($inputstring);
}
	$store = new Dbms( );
	$store->OpenDatabase ( );						//  Connessione al Database 
	$iduser = userLogin ( );
	
	$track = new Track ( );
	$trkind = 1;
	$trkend = $track->trk_total ( );
	
	if ( $idplay == 0 )
		$idplay = $store->InsertInto ("mf_playlist (iduser,playname)", "($iduser,'$listname')");
 
	while ( $trkind <= $trkend )
	{
		if ( $track->trk_ischecked ( $trkind ) )
		{
			$track->trk_findimage ( $trkind, $artstore, $imgstore );
			$query = "SELECT idphoto FROM mf_photos WHERE photolink='$imgstore'";
			$result = $store->RunQuery ( $query );
			if ( ($found = $store->NextItem ($result)) )
				$idphoto = $found[0];
			else
				$idphoto = $store->InsertInto ("mf_photos (artist, photolink)",
											   "('$artstore', '$imgstore')");
			
			$track->trk_actual ( $trkind );
			$query = "SELECT idsong FROM mf_songs WHERE songlink='$track->save'";
			$result = $store->RunQuery ( $query );
			if ( ($found = $store->NextItem ($result)) )
				$idsong = $found[0];
			else
				$idsong = $store->InsertInto ("mf_songs (idphoto, songtitle, songlink)", 
											  "($idphoto, '$track->title', '$track->save')");
			
			$query = "SELECT * FROM mf_bridge 
					  INNER JOIN mf_playlist ON mf_playlist.idplay=mf_bridge.idplay 
					  WHERE mf_playlist.iduser=$iduser AND mf_bridge.idplay=$idplay 
					  AND mf_bridge.idsong=$idsong";
			$result = $store->RunQuery ( $query );
			if ( ! $store->HayResults ($result) )
				$store->InsertInto ("mf_bridge (idplay, idsong)", "($idplay, $idsong)");
				
			$track->trk_check ( $trkind, false );
		}
		$trkind ++;
	}	
	$store->CloseDatabase ();
	unset ( $store );
	unset ( $track );
?>
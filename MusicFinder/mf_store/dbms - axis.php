<?PHP
/*
    DATA ACCESS LAYER - DataBase Management
*/
class Dbms
{
//      Apre il database di nome DBname ed esegue i controlli di base
    function OpenDatabase ( )
    {
        $dbhostname = "musicalibera.tk";
        $dbname = "awxmwksq_musicfind";
        $dbusername = "awxmwksq";
        $dbpassword = "j0NV5rx32r";

        $dbconnect = mysqli_connect ($dbhostname, $dbusername, $dbpassword);
        if ( ! $dbconnect ) {
            die('Connection not established : ' . mysqli_error());
        }
//      Salvo il pointer di connessione nella Session
        $_SESSION["dbconnect"] = $dbconnect;

        $dbselected = mysqli_select_db ($dbconnect, $dbname);
        if ( ! $dbselected ) {
            die ('Database not connected : ' . mysqli_error());
        }
    }

//      Effettua una query di ricerca (impostata in DBbuffer)
    function RunQuery ( $dbbuffer )
    {
        return mysqli_query ($dbbuffer);
    }

//      Restituisce la riga successiva della query di ricerca precedente
    function NextItem ( $dbview )
    {
        if ( $dbview != Null )
            return mysqli_fetch_row ($dbview);
         else
            return Null;
    }

//      Restituisce il numero di record che soddisfano la ricerca precedente
    function HowManyItems ( $dbview )
    {
        if ( $dbview != Null )
            return mysqli_num_rows ($dbview);
        else
            return 0;
    }

//      Verifica se il risultato di una query è nullo
    function HayResults ( $dbview )
    {
        if ( mysqli_num_rows ($dbview) == 0 )
            return false;
        else
            return true;
    }

//      Inserisce un nuovo record in una data tabella
    function InsertInto ( $dbfields, $dbvalues )
    {
        $querystr = "INSERT INTO $dbfields  VALUES $dbvalues";

        ( $result = mysqli_query ($querystr) )
            or die("Query di Insert non valida: " . mysqli_error());

        return mysqli_insert_id();
    }
    
//      Update di un record esistente in una data tabella
    function UpdateTable ( $dbtable, $dbvalues, $dbcond )
    {
        $querystr = "UPDATE $dbtable SET $dbvalues WHERE $dbcond";

        ( $result = mysqli_query ($querystr) )
            or die("Query di Update non valida: " . mysqli_error());
    }

//      Cancellazione di un record esistente in una data tabella
    function DeleteRecord ( $dbtable, $dbcond )
    {
        $querystr = "DELETE FROM $dbtable WHERE $dbcond";

        ( $result = mysqli_query ($querystr) )
            or die("Query di Delete non valida: " . mysqli_error());
    }

//      Chiusura del DataBase precedentemente aperto
    function CloseDatabase ()
    {
        $dbconnect = $_SESSION["dbconnect"];
        if ( ! mysqli_close($dbconnect) ) {
            die('DB Close impossible : ' . mysqli_error());
        }
    }
}
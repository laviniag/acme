$(function () {
$('a[href="#search"]').on('click', function(event) {
// event.preventDefault();
$('#search').addClass('open');
$('#search > form > input[type="search"]').focus();
});

$('#search button.close').on('click', function() {
	$('#search').removeClass('open');
});

$('#search').on('click keyup', function(event) {
	if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
		$('#search').removeClass('open');
}
});

$('#search > form').submit(function(event) {
	$('#search').removeClass('open');
	window.input_search.submit();
});

});


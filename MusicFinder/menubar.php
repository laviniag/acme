<?php
if ( userLogin () > 0 ) {
	$marklist = "butt-show";
	$markout = "butt-show";
	$markin = "butt-hide";
} else {
	$marklist = "butt-hide";
	$markout = "butt-hide";
	$markin = "butt-show";	
}
?>
<!-- Enable Bootstrap-submenu via JavaScript:  -->
<script>  $('.dropdown-submenu > a').submenupicker();  </script>

<nav class="navbar navbar-default well text-center" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Music Finder</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav">
            <li><div class="input-group">
              <a href="#search">
                <button type="button" class="btn btn-sky to-stepdown">
                	<strong>&nbsp CERCA &nbsp</strong>
                	<i class="glyphicon glyphicon-search icon-search"></i>
                </button>
              </a>
            </div>
            </li> 
              
        	<li>
        		<button type="button" class="btn btn-light to-stepdown" onclick="loadFeatures()">
        			<strong>&nbsp ARTISTI IN EVIDENZA &nbsp</strong>
        		</button>
        	</li> 
<?php
			if ( trackTotal() > 0 ) {
?>
            	<li><button type="button" onclick="window.location='music.php'"
            	class="btn btn-fresh to-stepdown">
            		<strong>&nbsp RICERCA PRECEDENTE &nbsp</strong>
            	</button>
            	</li>  
<?php       }	?>   

            <li class="dropdown">
            <div id="play-list" class="<?= $marklist ?>">
                <button type="button" class="btn btn-sunny dropdown-toggle to-stepdown" 
                	data-toggle="dropdown"><i class="glyphicon glyphicon-list-alt"></i>
                	<strong>&nbsp LE TUE PLAYLIST &nbsp</strong><b class="caret"></b>
                </button>
                <ul id="list-name" class="dropdown-menu">
<?php
					require_once "./mf_store/insertplay.php";
?>             
                </ul>
            </div>
            </li>        
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
            <li>
            <div id="log-out" class="<?= $markout ?>">
               	<button type="button" onclick="inputLogout()" class="btn btn-sky to-stepdown">
            		<i class="glyphicon glyphicon-log-out"></i><strong>&nbsp LOGOUT &nbsp</strong>
            	</button>
            </div>
            </li>            

            <li class="dropdown">
            <div  id="log-in" class="<?= $markin ?>">
              	<button type="button" class="btn btn-sky dropdown-toggle to-stepdown" 
              		data-toggle="dropdown"><i class="glyphicon glyphicon-log-in"></i>
              		<strong>&nbsp LOGIN &nbsp</strong><b class="caret"></b>
			  	</button>
              	<ul class="dropdown-menu" style="padding: 15px;min-width: 250px;">
                  <li><div class="row">
            	  <form class="form" role="form" method="post" action="index.php" 
                  accept-charset="ISO-8859-15" id="login-nav">
                  <div class="col-md-12">
                    <div class="form-group">
                    	<label class="sr-only" for="email">E-mail</label>
                    	<input type="text" id="email" class="form-control" 
                    	 name="email" placeholder="E-mail" required>
                    </div>
                    <div class="form-group">
                    	<label class="sr-only" for="password">Password</label>
                    	<input type="password" id="password" class="form-control" 
                    	 name="password" placeholder="Password" required>
                    </div>
                    <div id="login-alert" class="my-alert"></div>
					
					<div class="form-group">
                    	<button type="button" onclick="inputControl(1)" 
                    	class="btn btn-success btn-block">Entra nel tuo Account</button>
                    </div>
                    <div class="form-group">
                    	<button type="button" onclick="inputControl(2)" 
                    	class="btn btn-success btn-block">Sono un Nuovo Utente</button>
                    </div>
            	  </div>
            	  </form> 
              	  </li>
          	  	</ul> 
          	</div> 
            </li>    

			<li><div>
            	<a class="iframe" href="images/guida.html" data-fancybox-type="iframe">
            	    <button type="button" class="btn btn-fresh to-stepdown">
            			<strong>&nbsp GUIDA &nbsp</strong>
            		</button>
            	</a>  
            </div>  	   
            </li> 
        </ul>
    </div> <!--/.navbar-collapse -->
</nav>

<div id="search">
	<button type="button" class="close"><i class="glyphicon glyphicon-remove-circle"></i></button>
	<form name="input_search" action="#">
    	<input type="search" placeholder="artista o titolo" name="singer" />
   		<button type="submit" class="btn btn-sky btn-lg">EFFETTUA RICERCA</button>
	</form>
</div>  
-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2014 at 11:10 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u128143833_mf`
--
CREATE DATABASE IF NOT EXISTS `awxmwksq_musicfind` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `awxmwksq_musicfind`;

-- --------------------------------------------------------

--
-- Table structure for table `mf_bridge`
--

CREATE TABLE IF NOT EXISTS `mf_bridge` (
  `idbridge` int(11) NOT NULL AUTO_INCREMENT,
  `idplay` int(11) NOT NULL,
  `idsong` int(11) NOT NULL,
  PRIMARY KEY (`idbridge`),
  KEY `idplay` (`idplay`),
  KEY `idsong` (`idsong`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_photos`
--

CREATE TABLE IF NOT EXISTS `mf_photos` (
  `idphoto` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(127) NOT NULL,
  `photolink` varchar(127) NOT NULL,
  PRIMARY KEY (`idphoto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_playlist`
--

CREATE TABLE IF NOT EXISTS `mf_playlist` (
  `idplay` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `playname` varchar(64) NOT NULL,
  PRIMARY KEY (`idplay`),
  KEY `iduser` (`iduser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_songs`
--

CREATE TABLE IF NOT EXISTS `mf_songs` (
  `idsong` int(11) NOT NULL AUTO_INCREMENT,
  `idphoto` int(11) NOT NULL,
  `songtitle` varchar(255) NOT NULL,
  `songlink` varchar(255) NOT NULL,
  PRIMARY KEY (`idsong`),
  KEY `idphoto` (`idphoto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_users`
--

CREATE TABLE IF NOT EXISTS `mf_users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `nickname` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
